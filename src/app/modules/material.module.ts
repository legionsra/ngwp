import { NgModule } from '@angular/core';
import {
    MatButtonModule, 
    MatCheckboxModule,
    MatToolbarModule, 
    MatCardModule, 
    MatProgressSpinnerModule,
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
} from '@angular/material';
    

@NgModule({
imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
],
exports: [
    MatButtonModule, 
    MatCheckboxModule, 
    MatToolbarModule, 
    MatCardModule, 
    MatProgressSpinnerModule, 
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
],
})
export class MaterialModule { }
