import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';




@Component({
  selector: 'app-link',
  template: `
    <p>
      link works!
    </p>
  `,
  styles: [],
  encapsulation: ViewEncapsulation.Native
})
export class LinkComponent implements OnInit {
  @Input() eClasses;
  @Input() eId;
  @Input() eHref;

  constructor() { }

  ngOnInit() {
    
  }

}
