

export interface IWidget {
    name: string;
    id: string;
    classname: string;
    customize_selective_refresh: Boolean;
    description: string;
    rendered: string;
    component?: any;
    inputs?: any;
    outputs?: any;
}
