import { IWidget } from "./iwidget";

export interface ISidebar {
    name?: String;
    id?: String;
    description?: String;
    class?: String;
    before_widget?: String;
    after_widget?: String;
    before_title?: String;
    after_title?: String;
    rendered?: String;
    widgets?: IWidget[];
}
