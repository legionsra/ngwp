import { IPage } from "./ipage";
import { ISidebar } from "./isidebar";
import { IAPIPage } from "./API/iapipage";
import { IMenu } from "./imenu";

export interface IConfig {
    home_page?: IAPIPage | IPage | false;
    posts_page?: IAPIPage | IPage | false;
    options?: IOption;
    menus?: IMenus;
    initial_page?: IAPIPage | IPage | false;
    sidebars?: ISidebars;
}
interface IOption {
    ngwp_site_url?: string;
    ngwp_site_home?: string;
    ngwp_blogname?: string;
    ngwp_blogdesc?: string;
    ngwp_posts?: string;
    ngwp_cache?: boolean;

}
interface IMenus {
    header?: IMenu[] | false;
    footer?: IMenu[] | false;
}
interface ISidebars {
    vertical?: ISidebar;
}
