import { IPage } from "./ipage";

/**
 * Determine the interface of a single element in a menu
 *
 * @export
 * @interface IMenu
 */
export interface IMenu {
    ID: number;
    url: string;
    title: string;
    object: string;
    dropdown?: IMenu[];
    data?: IPage;
}
