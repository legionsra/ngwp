import { BehaviorSubject } from "../../../node_modules/rxjs";
import { IPage } from "./ipage";

export interface IPostsList {
    nElems: number;
    orderBy: string;
    order: string;
    content: BehaviorSubject<IPage[]>;
}
