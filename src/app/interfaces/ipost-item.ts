export interface IPostItem {
    id: number;
    title: string;
    date: Date;
    slug: string;
    status: string;
    type: string;
    featured_media?: IMedia[];
    excerpt?: string;
    url: string;
    content: string;
}
interface IMedia {
    id: number;
    sizes?: ISizeMedia;
    src: string;
}
interface ISizeMedia {
    [property:string]: {
        width: Number;
        height: Number;
        src: string;
    }

}
