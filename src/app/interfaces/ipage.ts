
export interface IPage {
    id: number;
    url: string;
    type: string;
    title: string;
    excerpt?: string;
    content: string;
    template?: "default" | "sidebar-left" | "no-sidebar" | "fullwidth-default" | "fullwidth-sidebar-left" | "fullwidth-no-sidebar" ;
    date?: Date;
    slug?: string;
    status?: string;
    featured_media?: IMedia[];
}
interface IMedia {
    id: number;
    sizes?: ISizeMedia;
    src: string;
}
interface ISizeMedia {
    [property:string]: {
        width: Number;
        height: Number;
        src: string;
    }

}
