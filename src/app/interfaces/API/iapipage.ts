export interface IAPIPage {
    id: number;
    date: string;
    date_gmt: string;
    guid: { rendered: string;};
    modified: string;
    modified_gmt: string;
    slug: string;
    status: string;
    type: string;
    link: string;
    title: { rendered: string; };
    content: {
        rendered: string;
        protected: boolean;
    };
    excerpt: {
        rendered: string;
        protected: boolean;
    };
    author: number;
    featured_media: number;
    comment_status: string;
    ping_status: string;
    sticky: boolean;
    template: string;
    format: string;
    meta: string[];
    categories: number[];
    tags: number[];
    // _links: {},
    _embedded: IEmbedded
}

interface IEmbedded {
    author: IAutor[],
    replies: IReplies[],
    wp_featuredmedia: IWPFeatureMedia[],
    wp_term: IWPTerm[]
}

interface IAutor {
    id: number;
    name: string;
    url: string;
    description: string;
    link: string;
    slug: string;
    avatar_urls: {[ids:number]:string};
    _links: {
        self: [{ href: string;}];
        collection: [{href: string;}];
    };
    
}
interface IReplies {
    
    id: number;
    parent: number;
    author: number;
    author_name: string;
    author_url: string;
    date: string;
    content: {rendered: string;};
    link: string;
    type: string;
    author_avatar_urls: {[ids:number]:string};
    _links: {
        self: [{ href: string;}];
        collection: [{href: string;}];
        up: [{
            embeddable: boolean;
            post_type: string;
            href: string;
        }];
    }
}

interface IWPFeatureMedia {
    id: number;
    date: string;
    slug: string;
    type: string;
    link: string;
    title: {rendered: string;};
    author: number;
    caption:  {rendered: string;};
    alt_text: string;
    media_type: string;
    mime_type: string;
    media_details: {
        width: number;
        height: number;
        file: string;
        sizes: {
            [imgFormat:string]: {
                file: string;
                width: number;
                height: number;
                mime_type: string;
                source_url: string;
            }
        },
        image_meta: {
            aperture: string;
            credit: string;
            camera: string;
            caption: string;
            created_timestamp: string;
            copyright: string;
            focal_length: string;
            iso: string;
            shutter_speed: string;
            title: string;
            orientation: string;
            keywords: any[];
        }
    },
    source_url: string;
    _links: {
        self: [{ attributes: any[]; href: string; }];
        collection: [{ attributes: any[]; href: string; }];
        about: [{ attributes: any[]; href: string; }];
        author: [{ attributes: {embeddable:boolean;}; href: string; }];
        replies: [{ attributes: {embeddable:boolean;}; href: string; }];
    }
    
}

interface IWPTerm {
    id: number;
    link: string;
    name: string;
    slug: string;
    taxonomy: string;
    _links: {
        self: [{ href: string; }];
        collection: [{ href: string; }];
        about: [{ href: string; }];
        wp_post_type: [{href: string;}];
        curies: [{
            name: string;
            href: string;
            templated: boolean;
        }];
    }
}
