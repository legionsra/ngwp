import { Injectable } from '@angular/core';
import { IPostItem } from '../interfaces/ipost-item';
import { IPage } from '../interfaces/ipage';
import { IConfig } from '../interfaces/iconfig';
import { ISidebar } from '../interfaces/isidebar';
import { IError } from '../interfaces/ierror';
import { Router } from '@angular/router';
import { IsService } from './helpers/is.service';
import { TransformToService } from './helpers/transform-to.service';
import { GeneralService } from './helpers/general.service';
// import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  private _performance: {name:String,data: number, end:boolean}[] =[];
  // private _loaders: Array<BehaviorSubject<{name: String, status: boolean}>> = [];
  constructor(
    private _router: Router,
    private _is: IsService,
    private _transformTo: TransformToService,
    private _g: GeneralService
    ) { }
  g = this._g;
  stripDomain(url: string) {
    return '/' + url.replace(/https?:\/\/[^\/]+/i, '').split('/').filter(x => x !== '').join('/');
  }
  transformPostListFromWordPressAPI(input: any[]): IPostItem[]{
    const output = input.map(x => this.indivTransformationPage.call(this, x))
    return output;
  }
  transformTo = this._transformTo;
  indivTransformationPage(rawInput) {
    const transformed: any = {
      id: rawInput.id,
      date: new Date(rawInput.date_gmt),
      title: rawInput.title.rendered || '',
      slug: rawInput.slug,
      status: rawInput.status,
      type: rawInput.type || '',
      content: rawInput.content.rendered || '',
      excerpt: rawInput.excerpt.rendered || '',
      url: this.stripDomain(rawInput.link),
      template: this.getTemplate(rawInput.template)
    }
    if (rawInput.featured_media !== 0 ) {
      transformed.featured_media = [];
      for(let media of rawInput['_embedded']['wp:featuredmedia']) {
        let sizesExtracted = {};
        for(let size in media.media_details.sizes){
          
          sizesExtracted[size] = {
            width: media.media_details.sizes[size].width,
            height: media.media_details.sizes[size].height,
            src: media.media_details.sizes[size].source_url,
          }; 
        }
        transformed.featured_media.push({
          id:media.id,
          src: media.source_url,
          sizes: sizesExtracted
        })
      }
    }
    return transformed;
  }
  getTemplate(template:string):string {
    return (template == '' || typeof template == 'undefined') ? 'default'
    : (template == 'template-fullwidth.php') ? 'fullwidth-default'
    : 'default';
  }
  speedOf(name: string, override = false) {
    const now = performance.now();
    const found = this._performance.find(el => el.name == name);
    const modifData = (typeof found == 'undefined') 
    ? this._performance 
    : this._performance.filter(el => el.name !== name);
    if(override || typeof found == 'undefined') {
      // case create new measurement
      const newData = {
        name,
        data: now,
        end:false
      }
      modifData.push(newData);
      this._performance = modifData;
      // this._performance.next(modifData);
    } else if(found.end !== true) {
      // case update time and log result      
      found.data = now - found.data;
      found.end = true;
      modifData.push(found);
      this._performance = modifData;
      // this._performance.next(modifData);
      console.log(`Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
      
    } else {
      // case log results without modification
      console.log(`Logged: Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
      
    }
  }
  is = this._is;
  isIPage(elem: any): elem is IPage {
    return (typeof elem == 'undefined' || typeof elem === 'boolean' || Object.keys(elem).length === 0 && elem.constructor === Object) 
    ? false : <IPage>elem.url !== undefined;
  }
  isIConfig(elem: any): elem is IConfig {
    return (typeof elem == 'undefined' || typeof elem === 'boolean' || Object.keys(elem).length === 0 && elem.constructor === Object) ? false 
    : elem.home_page !== undefined || 
    elem.posts_page !== undefined ||
    elem.options !== undefined ||
    elem.menus !== undefined ||
    elem.initial_page !== undefined;
  }
  isIError(elem: any): elem is IError {
    return (typeof elem == 'undefined' || typeof elem === 'boolean' || Object.keys(elem).length === 0 && elem.constructor === Object) ? false
      : elem.code !== undefined ||
        elem.message !== undefined;
  }
  isISidebar(elem: any): elem is ISidebar {
    return (typeof elem == 'undefined' || typeof elem === 'boolean' || Object.keys(elem).length === 0 && elem.constructor === Object) ? false 
    : elem.name !== undefined ||
    elem.id !== undefined ||
    elem.description !== undefined ||
    elem.class !== undefined ||
    elem.before_widget !== undefined ||
    elem.after_widget !== undefined ||
    elem.before_title !== undefined ||
    elem.after_title !== undefined ||
    elem.rendered !== undefined;
  }
  isEmpty(obj : Object): boolean {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }
  
  
  
  getUrlParts(fullyQualifiedUrl) {
    const url: any = {};
    const a : HTMLAnchorElement = document.createElement('a')
    a.href = 
      (
        fullyQualifiedUrl.indexOf('://') !== -1 ||
        fullyQualifiedUrl.split('/')[0].indexOf('.') == -1
      )  
      ? fullyQualifiedUrl : 'https://' + fullyQualifiedUrl
      
      // if doesn't start with something like https:// it's not a url, but try to work around that
      //     if (fullyQualifiedUrl.indexOf('://') == -1) {
      //       if(fullyQualifiedUrl.split('/')[0].indexOf('.') == -1) {
      //         a.href = fullyQualifiedUrl
      //       }else {
      //         tempProtocol = 'https://'
      //         a.href = tempProtocol + fullyQualifiedUrl
      //       }
      
      //     } else
      //         a.href = fullyQualifiedUrl
      
      
      const parts = a.hostname.split('.')
      // url.origin = tempProtocol ? "" : a.origin
      url.domain = a.hostname
      url.subdomain = parts[0]
      url.domainroot = ''
      url.domainpath = ''
      url.tld = '.' + parts[parts.length - 1]
      url.path = a.pathname.substring(1)
      url.query = a.search.substr(1)
      url.protocol = /* tempProtocol ? "" : */ a.protocol.substr(0, a.protocol.length - 1)
      url.port = /* tempProtocol ? "" : */ a.port ? a.port : a.protocol === 'http:' ? 80 : a.protocol === 'https:' ? 443 : a.port
      url.parts = parts
      url.segments = a.pathname === '/' ? [] : a.pathname.split('/').slice(1)
      url.params = url.query === '' ? [] : url.query.split('&')
      for (let j = 0; j < url.params.length; j++) {
        let param = url.params[j];
        let keyval = param.split('=')
        url.params[j] = {
          'key': keyval[0],
          'val': keyval[1]
        }
      }
      // domainroot
      if (parts.length > 2) {
        url.domainroot = parts[parts.length - 2] + '.' + parts[parts.length - 1];
        // check for country code top level domain
        if (parts[parts.length - 1].length == 2 && parts[parts.length - 1].length == 2)
        url.domainroot = parts[parts.length - 3] + '.' + url.domainroot;
      }
      // domainpath (domain+path without filenames) 
      if (url.segments.length > 0) {
        let lastSegment = url.segments[url.segments.length - 1]
        let endsWithFile = lastSegment.indexOf('.') != -1
        if (endsWithFile) {
          let fileSegment = url.path.indexOf(lastSegment)
          let pathNoFile = url.path.substr(0, fileSegment - 1)
          url.domainpath = url.domain
          if (pathNoFile)
          url.domainpath = url.domainpath + '/' + pathNoFile
        } else
        url.domainpath = url.domain + '/' + url.path
      } else
      url.domainpath = url.domain
      return url
      
    }

    /**
     * Returns if the given path might be reserved for Wordpress purposes
     *
     * @param {string} url
     * @returns {boolean}
     * @memberof HelperService
     */
    isReservedURL(url: string):boolean {
      url = (url.substr(0,1) !== '/' || url == '') ? '/' + url : url;
      return url.includes('wp-admin') ||
        url.includes('wp-login') ||
        url.includes('wp-include');
    }
    
    /**
     * Navigates to the given URL and prevents default behaviour
     *
     * @param {string} url
     * @param {Event} event
     * @memberof HelperService
     */
    fluxNavigate(url: string, event: Event) {
      event.preventDefault();
      this._router.navigate([url]);
    }
    
  }
  
  
  
  