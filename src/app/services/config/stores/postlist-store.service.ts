import { Injectable } from '@angular/core';
import { IPostsList } from 'src/app/interfaces/iposts-list';
import { Observable } from 'rxjs';
import { IPage } from 'src/app/interfaces/ipage';
import { HelperService } from '../../helper.service';

@Injectable({
  providedIn: 'root'
})
export class PostlistStoreService {
  private _postsList: IPostsList[] = [];
  constructor(private _h: HelperService) { }
  /**
   * Returns if exists an observable of the posts listed or false
   *
   * @param {number} nElems
   * @param {string} [order='desc']
   * @param {string} [orderBy='date']
   * @returns {(Observable<IPage[]> | false)}
   * @memberof PostlistStoreService
   */
  get(nElems: number, order = 'desc', orderBy = 'date'): Observable<IPage[]> | false {
    const found = this._postsList.find(x => x.nElems === nElems && x.order === order && x.orderBy === orderBy);
    return found !== undefined ? found.content.asObservable() : false;
  }

  /**
   * Returns true if it is saved in memory, false if is incorrect input or is found already.
   *
   * @param {IPostsList} newList
   * @returns {boolean}
   * @memberof PostlistStoreService
   */
  save(newList: IPostsList) : boolean {
    if(this._h.is.IPostsList(newList)){
      const found = this._postsList.find(x => x.nElems === newList.nElems && x.order === newList.order && x.orderBy === newList.orderBy);
      if(!found) {
        this._postsList.push(newList);
        return true;
      }
      return false;
    }
    console.error(`PostsList given does not have the correct POSTSLIST's format: '${JSON.stringify(newList)}'`);
    return false;
  }
  
}
