import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ISidebar } from 'src/app/interfaces/isidebar';
import { HelperService } from '../../helper.service';

@Injectable({
  providedIn: 'root'
})
export class SidebarStoreService {
  private _sidebars: {[name:string]: BehaviorSubject<ISidebar>} = {};
  constructor(private _h: HelperService) { }

  /**
   *  Returns if exists an observable of the sidebar requested or false 
   *
   * @param {string} location
   * @returns {(Observable<ISidebar> | false)}
   * @memberof SidebarStoreService
   */
  get(location:string): Observable<ISidebar> | false {
    return this._sidebars.hasOwnProperty(location) 
          ? this._sidebars[location].asObservable()
          : false;
  }
  /**
   * Returns true if it is saved in memory, false if is incorrect input or is found already.
   *
   * @param {ISidebar} sidebar
   * @param {string} location
   * @returns {boolean}
   * @memberof SidebarStoreService
   */
  save(sidebar :ISidebar, location: string): boolean {
    console.log(sidebar);
    console.log(location);
    console.log(this._sidebars);
    
    
    if(this._h.is.ISidebar(sidebar)) {
      if(!this._sidebars.hasOwnProperty(location)){
        this._sidebars[location] = new BehaviorSubject<ISidebar>(sidebar)
        // this._sidebars[location].next(sidebar);
        return true;
      }
      return false; //Exists already
    }
    console.error(`Sidebar given does not have the correct SIDEBAR's format: '${JSON.stringify(sidebar)}'`);
    return false;
  }
}
