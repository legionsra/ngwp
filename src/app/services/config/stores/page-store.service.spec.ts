import { TestBed } from '@angular/core/testing';

import { PageStoreService } from './page-store.service';

describe('PageStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PageStoreService = TestBed.get(PageStoreService);
    expect(service).toBeTruthy();
  });
});
