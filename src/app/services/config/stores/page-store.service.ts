import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IPage } from 'src/app/interfaces/ipage';
import { HelperService } from '../../helper.service';

@Injectable({
  providedIn: 'root'
})
export class PageStoreService {
  private _pageData = new BehaviorSubject<IPage[]>([]);
  private _pageAsyncData = new BehaviorSubject<string[]>([]);
  constructor(private _h: HelperService) { }

  /**
   * Returns an observable with the pages stored
   *
   * @memberof PageStoreService
   */
  $data = this._pageData.asObservable();
  /**
   * Returns an observable with the url async stored
   *
   * @memberof PageStoreService
   */
  $dataAsync = this._pageAsyncData.asObservable();



  /**
   * Returns the actual list of pages stored
   *
   * @returns {IPage[]}
   * @memberof PageStoreService
   */
  getPages(): IPage[] {
    return this._pageData.getValue();
  }
  /**
   * Returns the actual list of pages stored
   *
   * @returns {string[]}
   * @memberof PageStoreService
   */
  getAsyncPages(): string[] {
    return this._pageAsyncData.getValue();
  }

  /**
   * Returns an expecific Page or undefined if not found
   *
   * @param {string} value
   * @param {string} [param='url']
   * @returns {(IPage | undefined)}
   * @memberof PageStoreService
   */
  getPageBy(value: string, param = 'url') : IPage | undefined {
    return this._pageData.getValue().find(p => p[param] === value);
  }
  /**
   * Return true if page is added to the storage
   *
   * @param {IPage} page
   * @returns {boolean}
   * @memberof PageStoreService
   */
  add(page: IPage): boolean {
    // console.log(page);
    
    if(this._h.is.IPage(page)) {
      // console.log('is PAGE');
      
      const listPages = this._pageData.getValue();
      const found = listPages.find(el => el.id > 0 && el.id === page.id && el.type === page.type || el.url === page.url);
      if(typeof found === 'undefined') {
        listPages.push(page);
        this._pageData.next(listPages);
        return true;
      }
      
      return false;
    }
    console.error(`Page given does not have the correct PAGE's format: '${JSON.stringify(page)}'`);
    return false;
  }
  /**
   * Return true if url page is added to the storage
   *
   * @param {IPage} page
   * @returns {boolean}
   * @memberof PageStoreService
   */
  addAsync(url: string): boolean {
    if(typeof url !== 'string' || url === '' || url.length === 0) {
      console.error(`URL given does not have the correct PATH's format: '${url}'`);
      return false;
    }
    // console.log(`URL: ${url}`);
    
    const path = this._h.g.getUrlParts(url).path;
    // console.log(this._h.g.getUrlParts(url));
    
    // console.log(`PATH: ${path}`);
    
    // * Return false if the URL is forbidden
    if(this._h.is.reservedURL(url)) return false;
    // * Return TRUE if is already on the client
    const found = this._pageData.getValue().some(el => el.url === path);
    if (found) return true;
    // * Return TRUE if is already on the queue
    const foundAsync = this._pageAsyncData.getValue().some(el => el == path);
    if (foundAsync) return true;

    const newAsyncList = this.getAsyncPages();
    newAsyncList.push(path);
    this._pageAsyncData.next(newAsyncList);
    return true;
  }

  resetAsync(): boolean {
    if(this.getAsyncPages().length == 0) {
      console.warn('Async URLs were already empty')
      return false;
    }
    this._pageAsyncData.next([]);
    return true
  }
}
