import { TestBed } from '@angular/core/testing';

import { MenuStoreService } from './menu-store.service';

describe('MenuStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuStoreService = TestBed.get(MenuStoreService);
    expect(service).toBeTruthy();
  });
});
