import { TestBed } from '@angular/core/testing';

import { SidebarStoreService } from './sidebar-store.service';

describe('SidebarStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SidebarStoreService = TestBed.get(SidebarStoreService);
    expect(service).toBeTruthy();
  });
});
