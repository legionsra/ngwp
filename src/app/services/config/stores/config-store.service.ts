import { Injectable } from '@angular/core';

import { IConfig } from '../../../interfaces/iconfig';
import { HelperService } from '../../helper.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigStoreService {
  private _config = new BehaviorSubject<IConfig>({});

  private _loadedConfig = new BehaviorSubject<boolean>(false);
  constructor(private _h: HelperService) { }

  /**
   * Get Observable of Config Content
   *
   * @memberof ConfigStoreService
   */
  $data = this._config.asObservable();

  /**
   * Get Observable of Config Loaded status
   *
   * @memberof ConfigStoreService
   */
  $loaded = this._loadedConfig.asObservable();
  
  /**
   * Returns the actual config
   *
   * @returns {IConfig}
   * @memberof ConfigStoreService
   */
  getSync() : IConfig {
    return this._config.getValue();
  }
  /**
   * Returns the actual loaded status of the config
   *
   * @returns {boolean}
   * @memberof ConfigStoreService
   */
  isLoaded() : boolean {
    return this._loadedConfig.getValue();
  }

  /**
   *  Return true if the Config has been stored correctly
   *
   * @param {IConfig} conf
   * @returns {boolean}
   * @memberof ConfigStoreService
   */
  save(conf: IConfig) : boolean {
    if(this._h.is.IConfig(conf)) {
      this._config.next(conf);
      this._loadedConfig.next(true);
      return true;
    }
    return false;
  }


}