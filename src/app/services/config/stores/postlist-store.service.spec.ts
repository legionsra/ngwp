import { TestBed } from '@angular/core/testing';

import { PostlistStoreService } from './postlist-store.service';

describe('PostlistStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostlistStoreService = TestBed.get(PostlistStoreService);
    expect(service).toBeTruthy();
  });
});
