import { Injectable } from '@angular/core';
import { IMenu } from '../../../interfaces/imenu';
import { BehaviorSubject, Observable } from 'rxjs';
import { HelperService } from '../../helper.service';


@Injectable({
  providedIn: 'root'
})
export class MenuStoreService {
  private _menus: {[name:string]: BehaviorSubject<IMenu[]>} = {};


  constructor(private _h: HelperService) { }

  /**
   * Returns the menu array as an Observable, or it returns false
   *
   * @param {string} location
   * @returns {(Observable<IMenu[]> | boolean)}
   * @memberof MenuStoreService
   */
  get(location: string): Observable<IMenu[]> | false {
    return this._menus.hasOwnProperty(location) ? this._menus[location].asObservable() : false;
  }
  /**
   *  Returns the menu array if existss or it returns false
   *
   * @param {string} location
   * @returns {(IMenu[] | boolean)}
   * @memberof MenuStoreService
   */
  getSync(location:string): IMenu[] | boolean {
    return this._menus.hasOwnProperty(location) ? this._menus[location].getValue() : false;
  }

  /**
   * Return true if the menu has been stored correctly
   *
   * @param {IMenu[]} elemsMenu
   * @param {string} location
   * @returns {boolean}
   * @memberof MenuStoreService
   */
  save(elemsMenu: IMenu[], location: string) : boolean {
    if(!elemsMenu || elemsMenu.length == 0) return false;
    const passed = elemsMenu.reduce((status, menu) => this._h.is.IMenu(menu) ? status : false , true)
    
    if(passed) {
      // console.log(location);
      if(this._menus.hasOwnProperty(location)){
        this._menus[location].next(elemsMenu);
      } else {
        this._menus[location] = new BehaviorSubject<IMenu[]>(elemsMenu);
      }
      return true;
    }
    
    console.error(`Menu '${location}' does not have the correct MENU's format`);
    console.error(elemsMenu);
    
    return false;
    
  }
}
