import { TestBed } from '@angular/core/testing';

import { ConfigStoreService } from './config-store.service';

describe('ConfigStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigStoreService = TestBed.get(ConfigStoreService);
    expect(service).toBeTruthy();
  });
});
