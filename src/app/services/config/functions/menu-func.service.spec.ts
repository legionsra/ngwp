import { TestBed } from '@angular/core/testing';

import { MenuFuncService } from './menu-func.service';

describe('MenuFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuFuncService = TestBed.get(MenuFuncService);
    expect(service).toBeTruthy();
  });
});
