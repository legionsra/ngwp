import { TestBed } from '@angular/core/testing';

import { PostlistFuncService } from './postlist-func.service';

describe('PostlistFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostlistFuncService = TestBed.get(PostlistFuncService);
    expect(service).toBeTruthy();
  });
});
