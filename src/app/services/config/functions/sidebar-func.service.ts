import { Injectable } from '@angular/core';
import { ISidebar } from 'src/app/interfaces/isidebar';
import { Observable, of } from 'rxjs';
import { SidebarStoreService } from '../stores/sidebar-store.service';
import { ConfigStoreService } from '../stores/config-store.service';
import { WordpressService } from '../../wordpress.service';
import { switchMap, finalize, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SidebarFuncService {
  private _awaitingSidebar: Observable<ISidebar>;
  constructor(
    private _wp:WordpressService,
    private _config: ConfigStoreService,
    private _storeSidebar:SidebarStoreService,
  ) { }
  public retrieve(location: string): Observable<ISidebar> {
    // ### Case stored
    const found = this._storeSidebar.get(location);
    if(found) return found;
    
    // ### Case waiting response
    if(this._awaitingSidebar) return this._awaitingSidebar;

    if(this._config.isLoaded()) {
      // ### Case data is in the initial config
      if(this._config.getSync().sidebars.hasOwnProperty(location) && this._config.getSync().sidebars[location]) {
        if(this._storeSidebar.save(this._config.getSync().sidebars[location], location)) return <Observable<ISidebar>>this._storeSidebar.get(location);
        // At this point, if data is not returned, there is a misconfiguration in the config.
        console.warn('There may be a misconfiguration in the original "Config" request');
        console.warn(this._config.getSync().sidebars[location]);
      }
      
      // ### Case request new menu to server
      this._awaitingSidebar = this._wp.getSidebar(location).pipe(
        switchMap((sidebar:ISidebar) => {
          if(this._storeSidebar.save(sidebar,location)) return <Observable<ISidebar>>this._storeSidebar.get(location);
          return of({});
          
        }),
        finalize(() => this._awaitingSidebar = null),
        share()
      )
      return this._awaitingSidebar;
    } else {
      // Request menus to a new config or return empty
      //TODO: Request sidebar to a new config
      return of({});
    }

  }
}
