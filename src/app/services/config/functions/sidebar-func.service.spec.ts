import { TestBed } from '@angular/core/testing';

import { SidebarFuncService } from './sidebar-func.service';

describe('SidebarFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SidebarFuncService = TestBed.get(SidebarFuncService);
    expect(service).toBeTruthy();
  });
});
