import { Injectable } from '@angular/core';
import { WordpressService } from '../../wordpress.service';
import { MenuStoreService } from '../stores/menu-store.service';
import { Observable, of } from 'rxjs';
import { IMenu } from 'src/app/interfaces/imenu';
import { finalize, switchMap, share } from 'rxjs/operators';
import { ConfigStoreService } from '../stores/config-store.service';

@Injectable({
  providedIn: 'root'
})
export class MenuFuncService {
  private _awaitingMenu: Observable<IMenu[]>;
  constructor(
    private _wp:WordpressService,
    private _config: ConfigStoreService,
    private _storeMenus:MenuStoreService,

  ) { }
  



  public retrieve(location: string): Observable<IMenu[]> {
    // ### Case stored
    const found = this._storeMenus.get(location);
    if(found) return found;
    
    // ### Case waiting response
    if(this._awaitingMenu) return this._awaitingMenu;

    if(this._config.isLoaded()) {
      // ### Case data is in the initial config
      if(this._config.getSync().menus.hasOwnProperty(location) && this._config.getSync().menus[location]) {
        if(this._storeMenus.save(this._config.getSync().menus[location], location)) return <Observable<IMenu[]>>this._storeMenus.get(location);
        // At this point, if data is not returned, there is a misconfiguration in the config.
        console.warn('There may be a misconfiguration in the original "Config" request');
        console.warn(this._config.getSync().menus[location]);
      }
      
      // ### Case request new menu to server
      this._awaitingMenu = this._wp.getMenu(location).pipe(
        switchMap((arrMenus:IMenu[]) => {
          if(this._storeMenus.save(arrMenus,location)) return <Observable<IMenu[]>>this._storeMenus.get(location);
          return of([]);
          
        }),
        finalize(() => this._awaitingMenu = null),
        share()
      )
      return this._awaitingMenu;
    } else {
      // Request menus to a new config or return empty
      //TODO: Request menus to config functions
      return of([]);
    }

  }
}
