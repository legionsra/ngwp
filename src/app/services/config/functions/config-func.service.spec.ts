import { TestBed } from '@angular/core/testing';

import { ConfigFuncService } from './config-func.service';

describe('ConfigFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigFuncService = TestBed.get(ConfigFuncService);
    expect(service).toBeTruthy();
  });
});
