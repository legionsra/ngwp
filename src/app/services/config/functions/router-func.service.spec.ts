import { TestBed } from '@angular/core/testing';

import { RouterFuncService } from './router-func.service';

describe('RouterFuncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RouterFuncService = TestBed.get(RouterFuncService);
    expect(service).toBeTruthy();
  });
});
