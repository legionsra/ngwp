import { Injectable } from '@angular/core';
import { PageStoreService } from '../stores/page-store.service';
import { Router } from '@angular/router';
import { HelperService } from '../../helper.service';
import { ConfigStoreService } from '../stores/config-store.service';
import { IPage } from 'src/app/interfaces/ipage';
import { PostsComponent } from 'src/app/components/page/posts/posts.component';
import { PageComponent } from 'src/app/components/page/page/page.component';
import { NotFound404Component } from 'src/app/components/page/not-found404/not-found404.component';
import { Observable, of } from 'rxjs';
import { debounceTime, tap, finalize, switchMap } from 'rxjs/operators';
import { LoadpageResolverService } from 'src/app/guards/loadpage-resolver.service';
import { WordpressService } from '../../wordpress.service';
import { Forbidden403Component } from 'src/app/components/page/forbidden403/forbidden403.component';
import { CategoryComponent } from 'src/app/components/page/category/category.component';

@Injectable({
  providedIn: 'root'
})
export class RouterFuncService {
  private _debounceTimeAsync = 3000;

  private _observableAsync: Observable<{url:string;content:IPage}[]>;
  constructor(
    private _storePages: PageStoreService,
    private _router: Router,
    private _h: HelperService,
    private _config: ConfigStoreService,
    private _wp: WordpressService
  ) {
    this._config.$loaded.pipe(
      switchMap(loaded => {
        // console.log(loaded);
        
        if(loaded){
          // this._conf_susc.unsubscribe();
          // TODO: create response for debounce in API
          // this._config.getSync().options.fluxon_debounce_async
          this._debounceTimeAsync = 3000;
          return this._storePages.$dataAsync.pipe(
            debounceTime(this._debounceTimeAsync),
            // tap(x => console.log(x)),
            switchMap(x => x.length > 0 ? this.retrieveAsync(): of([]))
          );
        }
        return of([]);
      })
      
      ).subscribe();
    // ).subscribe(loaded => {
    //   if(loaded){
    //     this._conf_susc.unsubscribe();
    //     // TODO: create response for debounce
    //     // this._config.getSync().options.fluxon_debounce_async
    //     this._debounceTimeAsync = 10000;
    //   }
    // });
    // // * We are setting a time period to wait until the pool of urls are filled and we are ready to request to the server
    // this._storePages.$dataAsync.pipe(
    //   // tap(() => console.log('updated 1')),
    //   debounceTime(this._debounceTimeAsync),
    //   // switchMap(() => this.retrieveAsync())
    //   // tap(x => console.log(x)),
    //   switchMap(x => x.length > 0 ? this.retrieveAsync(): of([]))
    // ).subscribe(x => console.log(x));
   }

  register(page:IPage): boolean {
    const registered = (page.type == 'custom') ? false : this._storePages.add(page);
    // console.log('registering...: '+ page.url);
    if(registered){
      // console.log('registered: '+ page.url);
      // console.log(page);
      

      const path = (page.url.substr(0,1) == '/') ? page.url.substring(1) : page.url;
      const component  = 
          (page.type === 'posts') ? PostsComponent 
        : ( page.type === 'page') ? PageComponent 
        : ( page.type === 'post') ? PageComponent
        : ( page.type === 'category') ? CategoryComponent
        : ( page.type === '403') ? Forbidden403Component 
        : NotFound404Component;
      const newRoute =  {
        path,
        component,
        resolve: {data: LoadpageResolverService },
      };
      if(path ===  "") newRoute['pathMatch'] = 'full';
      this._router.config.unshift(newRoute);
      this._router.resetConfig(this._router.config);
      return true;
    }
    return false
  }
  registerAsync(url: string):boolean {
    // console.log('registering: ' + url);
    const answer = this._storePages.addAsync(url);
    
    // console.log(answer);
    
    return answer;
  }

  retrieveAsync(url = ''): Observable<{url:string;content:IPage}[]> {
    // console.log('retrieve async function');
    if(this._observableAsync) return this._observableAsync;
    // if(url !== '') this._storePages.addAsync(url);
    if(url !== '') console.log(url);
    const urls = this._storePages.getAsyncPages();
    if(this._h.is.emptyArr(urls)) {
      console.log('array of urls is empty');
      return of([]);
    }
    this._observableAsync = this._wp.getPagesByURLs(urls).pipe(
      tap((response: Array<{url:string;content:IPage}>) => {
        // console.log(response);
        
        if(!this._h.is.emptyArr(response)) {
          
          response.forEach(el => {
            // console.log(el);
            
            if(this._h.is.IError(el.content)) {
              // console.log('error');
              // console.log(el);
              
              const forbiddenPage: IPage = {
                content: 'IRRELEVANT',
                url: el.url,
                id: -1,
                type: '403',
                title: 'Forbidden Page'
              }
              this.register(forbiddenPage);
            } 
            else this.register(el.content)
          });
          console.log(this._router.config);
          // console.log(this._storePages.getPages())
          
        }
      }),
      finalize(() => {
        this._storePages.resetAsync();
        this._debounceTimeAsync = null;
      })
    )
    return this._observableAsync;
      
    }
      /* switchMap((response: Array<{url:string;content:IPage}>) => {
        if(this._h.is.emptyArr(response)) return response;
        response.map
        response.forEach(newURL => {
          if(!this._h.is.IError(newURL.content)) {
            this.register(this._h.transformTo.IPage(newURL.content))
            // console.info(newURL)
          }
          // TODO: Create an storage of urls with ERROR
        });
      }) */
      /* ).subscribe((response: Array<{url:string;content:IAPIPage | IError}>) => {

      // const listPages = this._storePages.getPages();    // Updating old values in case there were changes
      response.forEach(newURL => {
        if(!this._h.is.IError(newURL.content)) {
          this.register(this._h.transformTo.IPage(newURL.content))
          // console.info(newURL)
        }

        // TODO: Create an storage of urls with ERROR
      });
      if(!this._storePages.resetAsync()) console.warn('the async urls are already deleted');
      
      // this.printMemory('AFTER ASYNC UPDATE');


    }) */

}
