import { Injectable } from '@angular/core';
import { IPostsList } from 'src/app/interfaces/iposts-list';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { IPage } from 'src/app/interfaces/ipage';
import { PostlistStoreService } from '../stores/postlist-store.service';
import { ConfigStoreService } from '../stores/config-store.service';
import { WordpressService } from '../../wordpress.service';
import { switchMap, finalize, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostlistFuncService {
  private _awaitingPostList: Observable<IPage[]>;
  constructor(
    private _storePostLists: PostlistStoreService,
    private _config: ConfigStoreService,
    private _wp: WordpressService
  ) { }
  public retrieve(nElems: number, order = 'desc', orderBy = 'date'): Observable<IPage[]> {
    // ### Case stored
    const found = this._storePostLists.get(nElems,order,orderBy);
    if(found) return found;
    
    // ### Case waiting response
    if(this._awaitingPostList) return this._awaitingPostList;

    if(this._config.isLoaded()) {
      /* 
      // ### Case data is in the initial config
      if(this._config.getSync().menus.hasOwnProperty(location) && this._config.getSync().menus[location]) {
        if(this._storePostLists.save(this._config.getSync().menus[location], location)) return <Observable<IMenu[]>>this._storePostLists.get(location);
        // At this point, if data is not returned, there is a misconfiguration in the config.
        console.warn('There may be a misconfiguration in the original "Config" request');
        console.warn(this._config.getSync().menus[location]);
      } */
      
      // ### Case request new menu to server
      this._awaitingPostList = this._wp.getPosts(nElems,order,orderBy).pipe(
        switchMap((arrPosts: IPage[]) => {
          if (arrPosts.constructor === Array) {
            const newPostList: IPostsList = {
              nElems,
              order,
              orderBy,
              content: new BehaviorSubject<IPage[]>(arrPosts)
            }
            
            if(this._storePostLists.save(newPostList)) 
              return <Observable<IPage[]>>this._storePostLists.get(nElems,order,orderBy);
          }
          return of([]);
          
        }),
        finalize(() => this._awaitingPostList = null),
        share()
      )
      return this._awaitingPostList;
    } else {
      // TODO: Create auto response included in Config for default postlist
      return of([]);
    }

  }
}
