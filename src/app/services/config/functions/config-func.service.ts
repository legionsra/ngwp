import { Injectable } from '@angular/core';
import { IConfig } from 'src/app/interfaces/iconfig';
import { Observable, of } from 'rxjs';
import { WordpressService } from '../../wordpress.service';
import { ConfigStoreService } from '../stores/config-store.service';
import { switchMap, finalize, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigFuncService {
  private _awaitingConfig: Observable<IConfig>;
  constructor(
    private _wp: WordpressService,
    private _storeConfig: ConfigStoreService
  ) { }
  /**
   * Public return observable boolean about the status of the initial config
   *
   * @memberof ConfigFuncService
   */
  public $loaded: Observable<boolean> = this._storeConfig.$loaded

  
  /**
   * Public safe function to retrieve an observable of IConfig
   *
   * @param {string} [url='']
   * @returns {Observable<IConfig>}
   * @memberof ConfigFuncService
   */
  public retrieve(url = '') : Observable<IConfig> {
     // ### Case stored
     const found = this._storeConfig.isLoaded();
     if(found) return this._storeConfig.$data;

     // ### Case waiting response
    if(this._awaitingConfig) return this._awaitingConfig;

    // ### Case request new config to server
    this._awaitingConfig = this._wp.getConf(url).pipe(
      switchMap((conf:IConfig) => {
        if(this._storeConfig.save(conf)) return <Observable<IConfig>>this._storeConfig.$data;
        return of({});
        
      }),
      finalize(() => this._awaitingConfig = null),
      share()
    )
    return this._awaitingConfig;
  }


  // TODO: fix return of getOption function
  /**
   * Return the requested option from the Config
   *
   * @param {string} opt
   * @returns {*}
   * @memberof ConfigFuncService
   */
  public getOption(opt:string): any {
    if(typeof opt == 'undefined' || typeof opt !== 'string') return console.error(`Incorrect getOption format: ${opt}`);
    ;
    if(!this._storeConfig.isLoaded()) return console.error('Options are not available yet');
    const options = this._storeConfig.getSync().options;
    if(options.hasOwnProperty(opt)) {
      return options[opt];
    }
    console.warn(`There is no option: ${opt}`);
    return false;
    
  }
}
