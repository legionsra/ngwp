import { TestBed, inject, async } from '@angular/core/testing';

import { HelperService } from './helper.service';
import { WordpressService } from './wordpress.service';
import { HttpClient, HttpParams, HttpClientModule } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { of } from 'rxjs';

fdescribe('HelperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: []
    });
  });

  it('should be created', inject([HelperService], (service: HelperService) => {
    expect(service).toBeTruthy();
  }));
  it('should Decode Wordpress API', async(inject([HelperService, HttpClient], (service: HelperService, http: HttpClient) => {
    const params = new HttpParams()
      .set('per_page','6')
      .set('order', 'asc')
      .set('orderby', 'date');
    let received = [];
    // let http: HttpClient;
    http.get<any[]>('http://localhost:80/wp-json/wp/v2/posts?_embed', {
      params: params
    }).pipe(
      catchError(err => of([])), 
      tap(x => this.received = x),
      map(x => service.transformPostListFromWordPressAPI(x)),
      /* tap(x => console.log(x)), */
    ).subscribe(x => {
      expect(x).not.toBe(received);
    })
    // http.get<any[]>(this.API_URL + '/wp-json/wp/v2/posts?_embed', {
    //   params: params
    // }).subscribe(x => console.log(x))
    // expect(service).toBeTruthy();
  })));

});
