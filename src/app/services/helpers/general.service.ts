import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class GeneralService {
	private _performance: {name:String,data: number, end:boolean}[] =[];
	constructor(
		private _router: Router
		) { }


	stripDomain(url: string) {
		return '/' + url.replace(/https?:\/\/[^\/]+/i, '').split('/').filter(x => x !== '').join('/');
	}


	/**
	* Navigates to the given URL and prevents default behaviour
	*
	* @param {string} url
	* @param {Event} event
	* @memberof HelperService
	*/
	fluxNavigate(url: string, event: Event) {
		event.preventDefault();
		const extraOptions = {
			// fragment: "main"
		}
		this._router.navigate([url],extraOptions);
	}
	
	
	getTemplate(template:string):"default" | "sidebar-left" | "no-sidebar" | "fullwidth-default" | "fullwidth-sidebar-left" | "fullwidth-no-sidebar" {
		return (template == '' || typeof template == 'undefined') ? 'default'
		: (template == 'template-fullwidth.php') ? 'fullwidth-default'
		: 'default';
	}
		
	getUrlParts(fullyQualifiedUrl) {
		const url: any = {};
		const a : HTMLAnchorElement = document.createElement('a')
		a.href = 
		(
			fullyQualifiedUrl.indexOf('://') !== -1 ||
			fullyQualifiedUrl.split('/')[0].indexOf('.') == -1
			)  
			? fullyQualifiedUrl : 'https://' + fullyQualifiedUrl
			
			// if doesn't start with something like https:// it's not a url, but try to work around that
			//     if (fullyQualifiedUrl.indexOf('://') == -1) {
			//       if(fullyQualifiedUrl.split('/')[0].indexOf('.') == -1) {
			//         a.href = fullyQualifiedUrl
			//       }else {
			//         tempProtocol = 'https://'
			//         a.href = tempProtocol + fullyQualifiedUrl
			//       }
			
			//     } else
			//         a.href = fullyQualifiedUrl
			
			
			const parts = a.hostname.split('.')
			// url.origin = tempProtocol ? "" : a.origin
			url.domain = a.hostname
			url.subdomain = parts[0]
			url.domainroot = ''
			url.domainpath = ''
			url.tld = '.' + parts[parts.length - 1]
			url.path = a.pathname.slice(-1) == '/' ? a.pathname.substring(1,a.pathname.length - 1) : a.pathname.substring(1)
			url.query = a.search.substr(1)
			url.protocol = /* tempProtocol ? "" : */ a.protocol.substr(0, a.protocol.length - 1)
			url.port = /* tempProtocol ? "" : */ a.port ? a.port : a.protocol === 'http:' ? 80 : a.protocol === 'https:' ? 443 : a.port
			url.parts = parts
			url.segments = a.pathname === '/' ? [] : a.pathname.split('/').slice(1)
			url.params = url.query === '' ? [] : url.query.split('&')
			for (let j = 0; j < url.params.length; j++) {
				let param = url.params[j];
				let keyval = param.split('=')
				url.params[j] = {
					'key': keyval[0],
					'val': keyval[1]
				}
			}
			// domainroot
			if (parts.length > 2) {
				url.domainroot = parts[parts.length - 2] + '.' + parts[parts.length - 1];
				// check for country code top level domain
				if (parts[parts.length - 1].length == 2 && parts[parts.length - 1].length == 2)
				url.domainroot = parts[parts.length - 3] + '.' + url.domainroot;
			}
			// domainpath (domain+path without filenames) 
			if (url.segments.length > 0) {
				let lastSegment = url.segments[url.segments.length - 1]
				let endsWithFile = lastSegment.indexOf('.') != -1
				if (endsWithFile) {
					let fileSegment = url.path.indexOf(lastSegment)
					let pathNoFile = url.path.substr(0, fileSegment - 1)
					url.domainpath = url.domain
					if (pathNoFile)
					url.domainpath = url.domainpath + '/' + pathNoFile
				} else url.domainpath = url.domain + '/' + url.path
			} else url.domainpath = url.domain
			// console.log(url);
			
			return url
			
		}
	speedOf(name: string, override = false) {
		const now = performance.now();
		const found = this._performance.find(el => el.name == name);
		const modifData = (typeof found == 'undefined') 
		? this._performance 
		: this._performance.filter(el => el.name !== name);
		if(override || typeof found == 'undefined') {
			// case create new measurement
			const newData = {
				name,
				data: now,
				end:false
			}
			modifData.push(newData);
			this._performance = modifData;
			// this._performance.next(modifData);
		} else if(found.end !== true) {
			// case update time and log result      
			found.data = now - found.data;
			found.end = true;
			modifData.push(found);
			this._performance = modifData;
			// this._performance.next(modifData);
			console.log(`Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
			
		} else {
			// case log results without modification
			console.log(`Logged: Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
			
		}
	}
}
		