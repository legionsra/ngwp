import { TestBed } from '@angular/core/testing';

import { TransformToService } from './transform-to.service';

describe('TransformToService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TransformToService = TestBed.get(TransformToService);
    expect(service).toBeTruthy();
  });
});
