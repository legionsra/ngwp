import { Injectable } from '@angular/core';
import { IPage } from 'src/app/interfaces/ipage';
import { IConfig } from 'src/app/interfaces/iconfig';
import { IError } from 'src/app/interfaces/ierror';
import { ISidebar } from 'src/app/interfaces/isidebar';
import { IMenu } from 'src/app/interfaces/imenu';
import { IPostsList } from 'src/app/interfaces/iposts-list';
import { IAPIPage } from 'src/app/interfaces/API/iapipage';
import { ICategory } from 'src/app/interfaces/icategory';

/**
 * Service to compare properties and interfaces and returns Booleans or Interfaces
 *
 * @export
 * @class IsService
 */
@Injectable({
    providedIn: 'root'
})
export class IsService {

    constructor() { }
    /**
     * Returns true if elem is IPage
     *
     * @param {*} elem
     * @returns {elem is IPage}
     * @memberof IsService
     */
    IPage(elem: any): elem is IPage {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('id') &&
            elem.hasOwnProperty('url') &&
            elem.hasOwnProperty('title') &&
            elem.hasOwnProperty('type') &&
            elem.hasOwnProperty('content');

    }
    /**
     * Return true if elem is IConfig
     *
     * @param {*} elem
     * @returns {elem is IConfig}
     * @memberof IsService
     */
    IConfig(elem: any): elem is IConfig {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('home_page') &&
            elem.hasOwnProperty('posts_page') &&
            elem.hasOwnProperty('initial_page') &&
            elem.hasOwnProperty('options') &&
            elem.hasOwnProperty('menus');

    }
    /**
     * Return true if elem is IError
     *
     * @param {*} elem
     * @returns {elem is IError}
     * @memberof IsService
     */
    IError(elem: any): elem is IError {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('code') &&
            elem.hasOwnProperty('message');
    }
    /**
     * Return true if elem is ISidebar
     *
     * @param {*} elem
     * @returns {elem is ISidebar}
     * @memberof IsService
     */
    ISidebar(elem: any): elem is ISidebar {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('name') &&
            elem.hasOwnProperty('id') &&
            elem.hasOwnProperty('description') &&
            elem.hasOwnProperty('class') &&
            elem.hasOwnProperty('before_widget') &&
            elem.hasOwnProperty('after_widget') &&
            elem.hasOwnProperty('before_title') &&
            elem.hasOwnProperty('after_title') &&
            elem.hasOwnProperty('rendered');
    }
    /**
     * Return true if elem is IMenu
     *
     * @param {*} elem
     * @returns {elem is IMenu}
     * @memberof IsService
     */
    IMenu(elem: any): elem is IMenu {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('title') &&
            elem.hasOwnProperty('ID') &&
            elem.hasOwnProperty('object') &&
            elem.hasOwnProperty('url');

    }
    /**
     * Return true if elem is IPostList
     *
     * @param {*} elem
     * @returns {elem is IPostsList}
     * @memberof IsService
     */
    IPostsList(elem: any): elem is IPostsList {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('nElems') &&
            elem.hasOwnProperty('orderBy') &&
            elem.hasOwnProperty('order') &&
            elem.hasOwnProperty('content');

    }
    /**
     * Return true if elem is IAPIPage
     *
     * @param {*} elem
     * @returns {elem is IAPIPage}
     * @memberof IsService
     */
    IAPIPage(elem: any): elem is IAPIPage {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('id') &&
            elem.hasOwnProperty('date') &&
            elem.hasOwnProperty('modified') &&
            elem.hasOwnProperty('slug') &&
            elem.hasOwnProperty('status') &&
            elem.hasOwnProperty('link') &&
            elem.hasOwnProperty('title') &&
            elem.hasOwnProperty('excerpt') &&
            elem.hasOwnProperty('author') &&
            elem.hasOwnProperty('content') &&
            elem.hasOwnProperty('featured_media') &&
            elem.hasOwnProperty('template') &&
            elem.hasOwnProperty('_embedded') &&
            elem.hasOwnProperty('type');

    }
    ICategory(elem: any): elem is ICategory {
        return (typeof elem == 'undefined' || typeof elem === 'boolean' || this.emptyObj(elem)) ? false
            : elem.hasOwnProperty('term_id') &&
            elem.hasOwnProperty('name') &&
            elem.hasOwnProperty('slug') &&
            elem.hasOwnProperty('term_group') &&
            elem.hasOwnProperty('term_taxonomy_id') &&
            elem.hasOwnProperty('taxonomy') &&
            elem.hasOwnProperty('description') &&
            elem.hasOwnProperty('parent') &&
            elem.hasOwnProperty('count') &&
            elem.hasOwnProperty('filter') &&
            elem.hasOwnProperty('cat_ID') &&
            elem.hasOwnProperty('category_count') &&
            elem.hasOwnProperty('category_description') &&
            elem.hasOwnProperty('cat_name') &&
            elem.hasOwnProperty('category_nicename') &&
            elem.hasOwnProperty('category_parent');

    }
    /**
     * Returns true if input is an Object and is empty
     *
     * @param {Object} obj
     * @returns {boolean}
     * @memberof IsService
     */
    emptyObj(obj: Object): boolean {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }
    /**
     * Returns true if input is an Array and is empty
     *
     * @param {Array<any>} array
     * @returns {boolean}
     * @memberof IsService
     */
    emptyArr(array: Array<any>): boolean {
        return typeof array !== "undefined" && array !== null && array.length != null && array.length == 0;
    }

    /**
       * Returns if the given path might be reserved for Wordpress purposes
       *
       * @param {string} url
       * @returns {boolean}
       * @memberof HelperService
       */
    reservedURL(url: string): boolean {
        url = (url.substr(0, 1) !== '/' || url == '') ? '/' + url : url;
        return url.includes('wp-admin') ||
            url.includes('wp-login') ||
            url.includes('wp-include');
    }
}
