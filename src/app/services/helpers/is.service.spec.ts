import { TestBed } from '@angular/core/testing';

import { IsService } from './is.service';

describe('IsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IsService = TestBed.get(IsService);
    expect(service).toBeTruthy();
  });
});
