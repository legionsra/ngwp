import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { IPage } from 'src/app/interfaces/ipage';
import { IAPIPage } from 'src/app/interfaces/API/iapipage';
import { IMenu } from 'src/app/interfaces/imenu';
import { IsService } from './is.service';
import { IConfig } from 'src/app/interfaces/iconfig';
import { ICategory } from 'src/app/interfaces/icategory';

@Injectable({
    providedIn: 'root'
})
export class TransformToService {

    constructor(
        private _g: GeneralService,
        private _is: IsService
    ) { }
    IConfig(raw: IConfig): IConfig {
        if (this._is.IConfig(raw)) {
            const conf = Object.assign({}, raw);
            conf.home_page = this._is.IAPIPage(conf.home_page) ? this.IPage(conf.home_page) : false;
            conf.initial_page = this._is.IAPIPage(conf.initial_page) ? this.IPage(conf.initial_page) : false;
            conf.posts_page = this._is.IAPIPage(conf.posts_page) ? this.IPage(conf.posts_page) : false;
            Object.entries(conf.menus).forEach(([location, elems]) => {
                if (elems && elems.length > 0) {

                    elems = elems.map(menuItem => this.IMenu.call(this, menuItem));

                    conf.menus[location] = elems
                }
            });
            return conf;
        }
        return {};
    }
    IMenu(raw: any): IMenu {

        const menu = Object.assign({}, raw);
        if (menu.object !== 'custom') {

            menu.url = this._g.stripDomain(raw.url);
            menu.data = this.IPage(menu.data);
        }

        return menu;
    }
    IPage(raw: IAPIPage): IPage {
        const transformed: IPage = {
            id: raw.id,
            date: new Date(raw.date_gmt),
            title: raw.title.rendered || '',
            slug: raw.slug,
            status: raw.status,
            type: raw.type || '',
            content: raw.content.rendered || '',
            excerpt: raw.excerpt.rendered || '',
            url: this._g.stripDomain(raw.link),
            template: this._g.getTemplate(raw.template)
        }
        if (raw.featured_media !== 0) {
            transformed.featured_media = [];
            for (let media of raw['_embedded']['wp:featuredmedia']) {
                let sizesExtracted = {};
                for (let size in media.media_details.sizes) {

                    sizesExtracted[size] = {
                        width: media.media_details.sizes[size].width,
                        height: media.media_details.sizes[size].height,
                        src: media.media_details.sizes[size].source_url,
                    };
                }
                transformed.featured_media.push({
                    id: media.id,
                    src: media.source_url,
                    sizes: sizesExtracted
                })
            }
        }
        return transformed;
    }
    IPageArray(raw: IAPIPage[]): IPage[] {
        const newIPageArr: IPage[] = [];
        raw.map((pageAPI: IAPIPage) => {
            newIPageArr.push(this.IPage(pageAPI));
        })
        return newIPageArr;
    }
    IPageICategoryPosts(raw: ICategory, url: string): IPage {
        const transformed: IPage = {
            id: raw.cat_ID,

            title: raw.name || '',
            slug: raw.slug,
            type: raw.taxonomy || '',
            content: raw.description || '',
            excerpt: raw.description || '',
            url,
        }

        return transformed;
    }
}
