import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { IPage } from '../interfaces/ipage';
import { IConfig } from '../interfaces/iconfig';
import { ISidebar } from '../interfaces/isidebar';
import { IMenu } from '../interfaces/imenu';

import { MenuFuncService } from './config/functions/menu-func.service';
import { RouterFuncService } from './config/functions/router-func.service';
import { PostlistFuncService } from './config/functions/postlist-func.service';
import { ConfigFuncService } from './config/functions/config-func.service';
import { SidebarFuncService } from './config/functions/sidebar-func.service';



@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  
  constructor(
    private c_menu: MenuFuncService,
    private c_dynRouter: RouterFuncService,
    private c_postList: PostlistFuncService,
    private c_config: ConfigFuncService,
    private c_sidebar: SidebarFuncService,
  ) {

    console.log('CONFIG INICIANDO');

  }
  registerPage(page: IPage): boolean {
    return this.c_dynRouter.register(page);
  }

  addURLToQueue(url: string):boolean {
    return this.c_dynRouter.registerAsync(url);
  }
  getAsyncPages(url = ''): Observable<{url:string;content:IPage}[]> {
    return this.c_dynRouter.retrieveAsync(url);
  }

  getMenu(location: string): Observable<IMenu[]> {
    return this.c_menu.retrieve(location);
  }

  getPostList(nElems: number,order?: string,orderBy?: string): Observable<IPage[]> { 
    return this.c_postList.retrieve(nElems,order,orderBy)
  }

  getSidebar(location: string): Observable<ISidebar> {
    return this.c_sidebar.retrieve(location);
  }

  getConf(url = ''): Observable<IConfig> {
    return this.c_config.retrieve(url);
  }
  getConfOpt(opt: string): any {
    return this.c_config.getOption(opt);
  }
  $confLoaded: Observable<boolean> = this.c_config.$loaded;

}
