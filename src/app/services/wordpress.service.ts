import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap, catchError, map, finalize } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { HelperService } from './helper.service';

import { IConfig } from '../interfaces/iconfig';
import { IMenu } from '../interfaces/imenu';
import { IAPIPage } from '../interfaces/API/iapipage';
import { IPage } from '../interfaces/ipage';
import { ISidebar } from '../interfaces/isidebar';
import { IError } from '../interfaces/ierror';

@Injectable()
export class WordpressService {
    // API_URL = 'http://demo.wp-api.org';
    // API_URL = 'https://revistaem.com';
    // API_URL = 'http://localhost:80';
    API_URL = '';
    // cache_Post_responses: any[];

    constructor(private http: HttpClient, private _h: HelperService) { }

    getPosts(nElems: number, order = 'desc', orderBy = 'date'): Observable<IPage[]> {
        // console.log('hey');
        this._h.speedOf(`LoadingPost<${nElems},${order},${orderBy}>`, true);
        const params = new HttpParams()
            .set('_embed', 'true')
            .set('per_page', nElems.toString())
            .set('order', order)
            .set('orderby', orderBy)
        // console.log('Getting content...')
        return this.http.get<IAPIPage[]>(this.API_URL + '/wp-json/wp/v2/posts', {
            // headers: {
            //   'Access-Control-Allow-Origin': '*'
            // },
            params: params
        }).pipe(
            catchError(err => {
                console.error(err);
                return of([])
            }),
            // tap(result => console.log(result)),
            map(posts => this._h.transformTo.IPageArray(posts)),
            finalize(() => this._h.speedOf(`LoadingPost<${nElems},${order},${orderBy}>`))
            // tap(x => console.log(x))
        );

    }
    getPage(type: string, id: number): Observable<IPage> {
        this._h.speedOf(`LoadingPage<${id}>`, true);
        return this.http.get<IAPIPage>(`${this.API_URL}/wp-json/ngwp/v1/${type}s/${id}?_embed`)
            .pipe(
                catchError(err => {
                    console.error(err);
                    return of({});
                }),
                map((pageAPI: IAPIPage) => this._h.transformTo.IPage(pageAPI)),
                finalize(() => this._h.speedOf(`LoadingPage<${id}>`))
            )
        // .pipe(tap(console.log));

    }
    getPagesByURLs(urls: string[]): Observable<{ url: string; content: IPage }[]> {
        this._h.speedOf(`LoadingPageByURLs<${urls.length}>`, true);
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        const parsedURLs = JSON.stringify({ url: urls });
        // console.log(parsedURLs);

        return this.http.post<Array<{ url: string; content: IAPIPage | IError }>>(`${this.API_URL}/wp-json/ngwp/v1/url`, parsedURLs, httpOptions)
            .pipe(
                catchError(err => {
                    console.error(err);
                    return of([])
                }),
                map((elems: Array<{ url: string; content: IAPIPage | IError }>) => {
                    console.log(elems);

                    if (this._h.is.emptyArr(elems)) return <any[]>elems;

                    const newElems: Array<{ url: string; content: IPage | IError }> = elems.reduce(
                        (result, el) => {
                            if (this._h.is.IAPIPage(el.content)) {
                                result.push({ url: el.url, content: this._h.transformTo.IPage(el.content) });
                            } else if (this._h.is.ICategory(el.content)) {
                                result.push({ url: el.url, content: this._h.transformTo.IPageICategoryPosts(el.content, el.url) });
                            } else {
                                result.push({ url: el.url, content: el.content })
                            }
                            return result;
                        }, []);
                    return newElems;
                }),
                finalize(() => this._h.speedOf(`LoadingPageByURLs<${urls.length}>`))
            )
        // .pipe(tap(console.log));

    }
    getConf(url: string): Observable<IConfig> {
        // console.log('ready for config server')
        url = (url.substr(0, 1) !== '/' || url == '') ? '/' + url : url;
        console.log(url);
        const params = new HttpParams()
            .set('url', url);
        this._h.speedOf('LoadingConfig', true);
        return this.http.get<IConfig>(this.API_URL + '/wp-json/ngwp/v1/config', {
            params
        })
            .pipe(
                // delay(10000),
                catchError(err => {
                    console.error(err);
                    console.error(err.status);
                    return of({});
                }),
                map(conf => this._h.transformTo.IConfig(conf)),
                finalize(() => this._h.speedOf('LoadingConfig'))
                // tap(x => console.log(x)),
                // shareReplay(1),
            );
    }
    getMenu(location: string): Observable<IMenu[]> {
        this._h.speedOf('LoadingMenu', true);
        return this.http.get<IMenu[]>(this.API_URL + '/wp-json/ngwp/v1/menu/' + location)
            .pipe(
                catchError(err => {
                    console.error(err);
                    return of([])
                }),
                map((menus: any[]) => {
                    if (menus.length > 0) {
                        menus.map(menu => this._h.transformTo.IMenu(menu));

                    } else return [];
                    return menus;
                    // return menus.map( menuItem:any =>
                    //   if (menuItem.hasOwnProperty('url')) {menuItem.url = this.h.stripDomain(menuItem.url); }
                    //   console.log(menuItem);
                    //   return menuItem;
                    // )
                }),
                // tap(x => console.log(x)),
                finalize(() => this._h.speedOf('LoadingMenu'))
            );
    }
    getSidebar(id: string): Observable<ISidebar> {
        this._h.speedOf('LoadingSidebar', true);
        return this.http.get<ISidebar>(this.API_URL + '/wp-json/ngwp/v1/sidebars/' + id)
            .pipe(
                catchError(err => {
                    console.error(err);
                    return of({})
                }),
                // map((sidebar: ISidebar) => {
                //   if (menus.length > 0) {
                //     menus.map(this._h.transformTo.IMenu);

                //   }
                //   return menus;

                // }),
                tap(x => console.log(x)),
                finalize(() => this._h.speedOf('LoadingSidebar'))
            );
    }
}
