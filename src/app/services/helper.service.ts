import { Injectable } from '@angular/core';
// import { IPostItem } from '../interfaces/ipost-item';
// import { IPage } from '../interfaces/ipage';
// import { IConfig } from '../interfaces/iconfig';
// import { ISidebar } from '../interfaces/isidebar';
// import { IError } from '../interfaces/ierror';
// import { Router } from '@angular/router';
import { IsService } from './helpers/is.service';
import { TransformToService } from './helpers/transform-to.service';
import { GeneralService } from './helpers/general.service';
// import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  private _performance: {name:String,data: number, end:boolean}[] =[];
  // private _loaders: Array<BehaviorSubject<{name: String, status: boolean}>> = [];
  constructor(
    // private _router: Router,
    private _is: IsService,
    private _transformTo: TransformToService,
    private _g: GeneralService
    ) { }
  g = this._g;
  
  transformTo = this._transformTo;
  
  speedOf(name: string, override = false) {
    const now = performance.now();
    const found = this._performance.find(el => el.name == name);
    const modifData = (typeof found == 'undefined') 
    ? this._performance 
    : this._performance.filter(el => el.name !== name);
    if(override || typeof found == 'undefined') {
      // case create new measurement
      const newData = {
        name,
        data: now,
        end:false
      }
      modifData.push(newData);
      this._performance = modifData;
      // this._performance.next(modifData);
    } else if(found.end !== true) {
      // case update time and log result      
      found.data = now - found.data;
      found.end = true;
      modifData.push(found);
      this._performance = modifData;
      // this._performance.next(modifData);
      console.log(`Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
      
    } else {
      // case log results without modification
      console.log(`Logged: Time for '${name}': ${(found.data / 1000).toFixed(2)}s`);
      
    }
  }
  is = this._is;
  
    
  }
  
  
  
  