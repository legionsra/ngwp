import { Pipe, PipeTransform, Inject } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { HelperService } from '../services/helper.service';

import { WINDOW } from '../providers/window.provider';
import { ConfigService } from '../services/config.service';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  constructor(
    protected sanitizer: DomSanitizer, 
    private _h: HelperService, 
    private _config: ConfigService,
    @Inject(WINDOW) private window: Window) {}
 
 public transform(value: string, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
  if (value == null) return '';
  let output = value;
  // console.log(value);
  
  // let input = value
  // console.log('before windows location');
  const href_regexs = /<a([^>]*?)href\s*=\s*(['"])([^\2]*?)\2\1*>/gi;
  const href_regex = /<a([^>]*?)href\s*=\s*(['"])([^\2]*?)\2\1*>/i;
  const anchorTags = value.match( href_regexs );
  const thisDomain = this.window.location.hostname;
  // console.log('after windows location');
  if(anchorTags !== null){
    for(let tag of anchorTags) {
      // console.log(tag);
      
      const anchorStruc = tag.match( href_regex );
      
      // console.log(arrayTag);
      const composedURL = this._h.g.getUrlParts(anchorStruc[3])
      // console.log(x);
      if(composedURL.domain == thisDomain) {
        //TODO: Register in router and create event and return anchor with click event
        // console.log(composedURL.path);
        // console.log('we are inside comparison');
        
        if(this._config.addURLToQueue(composedURL.path)) {
          // console.log('after adding to queue');
          
          const path = (composedURL.path.substr(0,1) !== '/') ? composedURL.path : composedURL.path.substr(1);
          const anchor = anchorStruc[0].slice(0, -1);
          const newAnchor = `${anchor} (click)="onClickAnchor('${path}',$event)">`
          // const newAnchor = `<a href="#" (click)="onClickAnchor('${path}',$event)" class="dam">`
          // console.log(newAnchor);
          // console.log(anchorStruc[0]);
          output = output.replace(anchorStruc[0], newAnchor);

          // console.log(output.indexOf(anchorStruc[0]));

          
        }
         

      }
      
      
      
      
    }
    // console.log(this.window.location.hostname);
    
    
  }
  else{
    // console.log('there is no anchor tags to parse');
    
  }
  

    switch (type) {
			case 'html': return this.sanitizer.bypassSecurityTrustHtml(output);
			case 'dynamic': return output;
			case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
			case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
			case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
			case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
			default: throw new Error(`Invalid safe type specified: ${type}`);
		}
  }
}
