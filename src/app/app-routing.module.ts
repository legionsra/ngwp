import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { HomeComponent } from './components/page/home/home.component';
import { UrlGuard } from './guards/url-guard.service';
// import { PageComponent } from './components/page/page/page.component';
import { LoadpageResolverService } from './guards/loadpage-resolver.service';
import { NotFound404Component } from './components/page/not-found404/not-found404.component';
// import { PostComponent } from './components/page/post/post.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: HomeComponent,
  //   canActivate: [UrlGuard],
  //   resolve: {data: LoadpageResolverService},
  //   pathMatch: 'full'
  // },
  {
    path: '**',
    canActivate: [UrlGuard],
    // redirectTo: '',
    component: NotFound404Component,
    // data: {data: 'initial'}
    resolve: {data: LoadpageResolverService}
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      // anchorScrolling: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
