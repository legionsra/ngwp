// fade.animation.ts

import { trigger, animate, transition, style, /* query */ } from '@angular/animations';

export const fadeAnimation =

    trigger('fadeAnimation', [
        // transition(':enter', [
        //     style({transform: 'translateY(-100%)'}),
        //     animate('200ms ease-in', style({transform: 'translateY(0%)'}))
        //   ]),
        transition(':leave', [
            style({ opacity: 1 }),
            animate('200ms ease-in', style({ opacity: 0 }))
        ])

        /* transition( '* => *', [

            // query(':enter', 
            //     [
            //         style({ opacity: 0 })
            //     ], 
            //     { optional: true }
            // ),

            query(':leave', 
                [
                    style({ opacity: 1 }),
                    animate('0.2s', style({ opacity: 0 }))
                ], 
                { optional: true }
            ),

            // query(':enter', 
            //     [
            //         style({ opacity: 0 }),
            //         animate('0.2s', style({ opacity: 1 }))
            //     ], 
            //     { optional: true }
            // )

        ]) */

    ]);