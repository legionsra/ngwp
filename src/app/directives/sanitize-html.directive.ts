import { Directive, Input, HostBinding } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Directive({
  selector: '[appSanitizeHtml]'
})
export class SanitizeHtmlDirective {
  @Input()
    public appSanitizeHtml: string;   

    @HostBinding('innerHtml')
    public get innerHtml(): SafeHtml {
      if(this.appSanitizeHtml !== undefined && this.appSanitizeHtml !== null){
        const href_regex = /<a([^>]*?)href\s*=\s*(['"])([^\2]*?)\2\1*>/gi;
        const anchorTags = this.appSanitizeHtml.match( href_regex );
        // console.log(href);
        if(anchorTags !== null){
          for(let tag of anchorTags) {
            console.log(tag);
            
          }
        }
        else{
          console.log('PAGEEEEEEEE');
          
        }
      }
      
      const htmlBypass = this._sanitizer.bypassSecurityTrustHtml(this.appSanitizeHtml);
      // console.log(htmlBypass);
      return htmlBypass
      
    }

  constructor(private _sanitizer: DomSanitizer) { }

}
