import { TestBed, inject } from '@angular/core/testing';

import { UrlGuard } from './url-guard.service';

describe('UrlGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlGuard]
    });
  });

  it('should be created', inject([UrlGuard], (service: UrlGuard) => {
    expect(service).toBeTruthy();
  }));
});
