import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, /* , RouterStateSnapshot, */ Router } from '@angular/router';
import { Observable, of } from 'rxjs';
// import { WordpressService } from '../services/wordpress.service';
import { ConfigService } from '../services/config.service';
import { switchMap } from 'rxjs/operators';
import { IConfig } from '../interfaces/iconfig';
// import { IPage } from '../interfaces/ipage';
import { HelperService } from '../services/helper.service';
import { IPage } from '../interfaces/ipage';
import { IMenu } from '../interfaces/imenu';
// import { delay } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UrlGuard implements CanActivate {
    configLoaded = false;
    constructor(
        private _router: Router,
        private _h: HelperService,
        /* private _wp: WordpressService, 
        */
        private _config: ConfigService
    ) {
        // console.log('GUARD INICIANDO');
    }

    canActivate(route: ActivatedRouteSnapshot,/*  state: RouterStateSnapshot */)
        : Observable<boolean> | Promise<boolean> | boolean {
        console.log('URL guard');
        // const slug = (route.url.length) ? route.url[route.url.length - 1].path : '';
        const url = (route.url.length) ? route.url.join('/') : '/';
        console.log(url);
        if (!this.configLoaded) {
            return this._config.getConf(url).pipe(
                switchMap((conf: IConfig) => {



                    if (this._h.is.IConfig(conf)) {
                        // Config exis and is not empty
                        console.log('gotten conf');
                        console.log(conf);

                        if (this._h.is.IPage(conf.home_page)) {
                            this._config.registerPage(conf.home_page);
                        } else {
                            const frontPage: IPage = {
                                content: conf.options.ngwp_blogdesc,
                                url: '/',
                                id: -1,
                                type: 'posts',
                                title: conf.options.ngwp_blogname
                            }
                            this._config.registerPage(frontPage);
                        }
                        if (this._h.is.IPage(conf.posts_page)) {
                            conf.posts_page.type = 'posts';
                            // console.log(conf.posts_page);

                            this._config.registerPage(conf.posts_page);
                        }

                        if (this._h.is.IPage(conf.initial_page)) this._config.registerPage(conf.initial_page);
                        Object.entries(conf.menus).forEach(([, elems]) => {
                            if (elems && elems.length > 0) {
                                elems.map((menuItem: IMenu) => {
                                    if (this._h.is.IPage(menuItem.data)) this._config.registerPage(menuItem.data);
                                });
                            }
                        });
                        /**
                        * EXPLANATION:
                        * IF homepage AND postspage ARE NOT DEFINED, DEFAULT URL IS postpage
                        * IF homepage IS DEFINED, DEFAULT URL IS homepage (BUT IS STILL POSIBLE TO HAVE postpage DEFINED ELSEWHERE)
                        * IF postpage IS DEFINED AND homepage NOT, DEFAULT URL IS postpage AS WELL AS THE URL OF postpage
                        */
                        if (url === '/') {
                            console.log('initial page');

                            // case default
                            console.log(conf.home_page)
                            console.log(this._h.is.IPage(conf.home_page))
                            if (this._h.is.IPage(conf.home_page)) {
                                // DO STUFF TO LOAD HOMEPAGE
                                console.log('Loading HomePage');
                                this._router.navigate([url]);

                            } else if (this._h.is.IPage(conf.posts_page)) {
                                console.log('Loading PostPage');
                                const frontPage = Object.assign({}, conf.posts_page);
                                frontPage.url = '/';
                                // conf.posts_page.url = '/';
                                this._config.registerPage(frontPage);
                                this._router.navigate([url]);
                            } else {
                                // console.error('Error loading config.\nNeither of HomePage or PostPage exists on the server');

                                this._router.navigate([url]);
                            }
                        }
                        else if (conf.initial_page && this._h.is.IPage(conf.initial_page)) {
                            // DO STUFF TO LOAD THE CUSTOM URL PAGE
                            console.log('Loading Custom URL Page');
                            this._router.navigate([url])
                        } else {
                            console.log(conf);

                            console.log('Loading 404 Page')
                            // DO STUFF TO LOAD 404 PAGE
                        }
                        console.log('FINISHED URL GUARD')
                        this.configLoaded = true;
                        return of(true);
                    }
                    console.log('Not gotten conf');
                    console.log(conf);


                    return of(false);
                }),
            )
        } else {
            console.log('Loading url after config');

            return this._config.getAsyncPages(url).pipe(
                switchMap(() => {
                    // console.log(x);

                    this._router.navigate([url]);
                    return of(true);
                })
            )
            // console.log('Perhaps you should wait until we have new async pages?');


        }
        // console.log(state);
        // return true;
    }
}
