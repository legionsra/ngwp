import { TestBed, inject } from '@angular/core/testing';

import { LoadpageResolverService } from './loadpage-resolver.service';

describe('LoadpageResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadpageResolverService]
    });
  });

  it('should be created', inject([LoadpageResolverService], (service: LoadpageResolverService) => {
    expect(service).toBeTruthy();
  }));
});
