import { Injectable, Injector } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, /* RouterStateSnapshot, Router */ } from '@angular/router';
// import { PageDataService } from '../services/config/page-data.service';
import { IPage } from '../interfaces/ipage';
import { Observable/* , of, BehaviorSubject */ } from 'rxjs';
import { PageStoreService } from '../services/config/stores/page-store.service';
// import { find, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoadpageResolverService implements Resolve<IPage> {

  constructor(/* private _pageData: PageStoreService, */ private injector: Injector/* , private _router: Router */) { }
  resolve(route: ActivatedRouteSnapshot, /* state: RouterStateSnapshot */): Observable<IPage> | IPage{
    // console.log('resolving!');
    const pageData = this.injector.get(PageStoreService);
    // console.log(route);
    
    const activeRouteURL = '/'+route.url.join('/');
    console.log(`Resolving to: ${activeRouteURL}`);
    const defaultPage: IPage = {
      id: 0,
      url: activeRouteURL,
      content: 'Default content',
      title: 'Default title',
      type: 'page',
      template: 'default'
    }
    // console.log(this._router.config)
    // const activeRoute
    // return this._pageData.getList().pipe(find(x => x.url === activeRouteURL),take(1));

    // return new BehaviorSubject<IPage>(this._pageData.getPage(activeRouteURL));
    const found = pageData.getPageBy(activeRouteURL, 'url');
    // TODO: Create Async Observable response when is still in process to get a new page
    // const asyncFound = pageData.getPageAsync(activeRouteURL);
    return typeof found !== 'undefined' 
      ? found : defaultPage;
      // ? found : typeof asyncFound !== 'undefined' 
      // ? pageData.getContentAsync(asyncFound) : defaultPage;
  }
}
