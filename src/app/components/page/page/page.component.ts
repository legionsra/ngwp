import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPage } from '../../../interfaces/ipage';
import { HelperService } from '../../../services/helper.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  page: IPage = {
    id: 1,
    title: 'Title Page',
    content: 'The Content',
    url: 'path',
    type: 'page',
    template: 'default'
  };
  textdf = '<p>s</p><app-link></app-link>';

  constructor(private route: ActivatedRoute, public _h: HelperService) { }

  ngOnInit() {
    this.route.data.subscribe((input: {data: IPage}) => {
 
      // console.log(input.data);
      
      this.page = input.data;
    })
  }
  isSidebarRight():boolean {
    return this.page.template == 'default' || this.page.template == 'fullwidth-default';
  }
  onClickAnchor(url: string, event: Event) {
    this._h.g.fluxNavigate(url,event);
  }

}
