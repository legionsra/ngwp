import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPage } from '../../../interfaces/ipage';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts = {
    col: 2,
    per_page: 10,
    gap: '16px',
    imgSize: 'medium'
  }
  page: IPage;


  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((input: {data: IPage}) => {
      this.page = input.data;
    })
  }
  isSidebarRight():boolean {
    return this.page.template == 'default' || this.page.template == 'fullwidth-default';
  }
}
