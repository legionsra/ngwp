import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
import { ISidebar } from '../../../interfaces/isidebar';
import { Observable, BehaviorSubject, Subscription, of } from 'rxjs';
import { HelperService } from '../../../services/helper.service';
import { IWidget } from 'src/app/interfaces/iwidget';
import { switchMap } from 'rxjs/operators';
import { WidgetVertCardDefaultComponent } from '../../molecule/widgets/vertical/card/widget-vert-card-default/widget-vert-card-default.component';
import { WidgetVertNocardDefaultComponent } from '../../molecule/widgets/vertical/nocard/widget-vert-nocard-default/widget-vert-nocard-default.component';
import { WidgetVertCardListComponent } from '../../molecule/widgets/vertical/card/widget-vert-card-list/widget-vert-card-list.component';
import { WidgetVertNocardListComponent } from '../../molecule/widgets/vertical/nocard/widget-vert-nocard-list/widget-vert-nocard-list.component';
import { WidgetVertCardSearchComponent } from '../../molecule/widgets/vertical/card/widget-vert-card-search/widget-vert-card-search.component';


@Component({
  selector: 'app-sidebar-vertical',
  templateUrl: './sidebar-vertical.component.html',
  styleUrls: ['./sidebar-vertical.component.scss']
})
export class SidebarVerticalComponent implements OnInit, OnDestroy {
    data : Observable<ISidebar>
    private _widgets = new BehaviorSubject<IWidget[]>([]);
    private _isLoading = new BehaviorSubject<boolean>(true);
    private widgetsStyle = 'card';
    private widget_susc: Subscription;
    
    isLoading = this._isLoading.asObservable();
    widgets = this._widgets.asObservable();
  constructor(
    private _config:ConfigService, 
    public _h: HelperService
    ) {

  }

  ngOnInit() {
    this.widget_susc = this._config.$confLoaded.pipe(
      switchMap((confReady:boolean) => confReady == true ? this._config.getSidebar('vertical') : of({}))
    ).subscribe((sidebar: ISidebar) => {
      if(this._h.is.ISidebar(sidebar)) {
        // TODO: get the option for type of widgets (Default: card)
        // console.log(this._config.getConfOpt('fluxon_widgets_vertical_style'));
        
        this.widgetsStyle = this._config.getConfOpt('fluxon_widgets_vertical_style') || 'card';
        const localWidgets = sidebar.widgets.map(widget => {
          
          widget.component = this.getTypeWidget(widget.classname,this.widgetsStyle);
          widget.inputs = {html: widget.rendered}
          return widget;
        });
        this._widgets.next(localWidgets);
        
        
      }

    })
   
    // this.widget_susc = this._config.getSidebar('vertical').subscribe((sidebar: ISidebar) => {

    // })
  }
  ngOnDestroy() {
    this.widget_susc.unsubscribe();
  }
  getTypeWidget(classW: string, styleW: string ){
    // console.log(classW);
    
      return (styleW == 'card') 
        ? (classW == 'widget_recent_entries' 
            || classW == 'widget_recent_comments'
            || classW == 'widget_archive' 
            || classW == 'widget_categories' 
            || classW == 'widget_meta') 
            ? WidgetVertCardListComponent
          : (classW == 'widget_search')
            ? WidgetVertCardSearchComponent
          : WidgetVertCardDefaultComponent
        : (classW == 'widget_recent_entries' 
            || classW == 'widget_recent_comments'
            || classW == 'widget_archive' 
            || classW == 'widget_categories' 
            || classW == 'widget_meta') 
            ? WidgetVertNocardListComponent
          : WidgetVertNocardDefaultComponent;

    
  }
  onClickAnchor(url: string, event: Event) {
    this._h.g.fluxNavigate(url,event);
  }

}
