import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
import { IMenu } from '../../../interfaces/imenu';
import { HelperService } from '../../../services/helper.service';
import { IConfig } from 'src/app/interfaces/iconfig';
import { Subscription, BehaviorSubject} from 'rxjs';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
// import { fadeAnimation } from 'src/app/animations/fade.animation';´




@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],

})
export class HeaderComponent implements OnInit, OnDestroy {
  private brand_susc: Subscription;
  private menu_susc : Subscription;
  title = 'Title';
  // menu = [];
  private _menus = new BehaviorSubject<IMenu[]>([]);
  private _brand = new BehaviorSubject<any>({}) ;
  menus = this._menus.asObservable();
  brand = this._brand.asObservable();

  headerImg;
  constructor(
    private _config: ConfigService, 
    private _h: HelperService,
    private _router: Router
    ) { }

  ngOnInit() {
    // console.log(this._router.url);
    this.headerImg = (this._router.url == '/') 
      ? {
        class: 'bg',
        src: 'url(https://placeimg.com/1920/1080/tech)'
      } : {
        class: 'no-bg',
        src: null
      };
    
    this.menu_susc = this._config.getMenu('header').subscribe((menus:IMenu[]) => {
      // console.log(menus);
      let newMenus: IMenu[] = [];
      if(menus.length > 0) newMenus = menus;
      else {
        newMenus = [{
            ID: 0,
            url: '/',
            title: 'menu 1',
            object: 'someObject'
          },{
            ID: 0,
            url: '/',
            title: 'menu 2',
            object: 'someObject'
          },{
            ID: 0,
            url: '/',
            title: 'menu 3',
            object: 'someObject'
          }]
      }
      this._menus.next(newMenus);
    });

    this.brand_susc = this._config.getConf().subscribe((conf:IConfig) => {
      
      if(!(Object.keys(conf).length === 0 && conf.constructor === Object)) {
        const newBrand = {
          title: conf.options.ngwp_blogname
        }
        // this._brand = new BehaviorSubject<any>(newBrand);
        this._brand.next(newBrand);
      }
    })
    this._router.events.subscribe(e => {
      if(e instanceof NavigationStart) {

        this.headerImg.class = (e.url !== '/') 
          ? 'no-bg'
          : 'bg';
      }
      if(e instanceof NavigationEnd) {
        if(e.url == '/') {
          this.headerImg.src = 'url(https://placeimg.com/1920/1080/tech)';
        }else {
          setTimeout(()=>{
            this.headerImg.src = null;
            // this.headerImg.src = (e.url !== '/') 
            //   ? null
            //   : 'url(https://placeimg.com/1920/1080/tech)';
          },400)

        }
        
      }
    })

    // this.brand_susc = this._config.$confLoaded.pipe(
    //   switchMap(loaded => (loaded) ? this._config.getConf() : of({})),
    // ).subscribe((conf:IConfig) => {
      
    //   if(!(Object.keys(conf).length === 0 && conf.constructor === Object)) {
    //     const newBrand = {
    //       title: conf.options.ngwp_blogname
    //     }
    //     // this._brand = new BehaviorSubject<any>(newBrand);
    //     this._brand.next(newBrand);
    //   }
    // })
    /* this.config.$confLoaded.subscribe(configReady => {
      if(configReady){
        // console.log('time to load menu')
        this.menu_susc = this.config.getMenu('header')
          .subscribe((data: any[]) => {
            // console.log('data')
            // console.log(data)
            this.menu.next(data)
            // this.menu = data;
          }, error => console.error(error));
      } else {
        // console.log('config not ready, waiting to load menu')
      }
    }) */
  }
  onClickMenu(url: string, event: Event) {
    this._h.g.fluxNavigate(url,event);
  }
  
  ngOnDestroy() { 
    this.menu_susc.unsubscribe();
    this.brand_susc.unsubscribe();
  }

}
