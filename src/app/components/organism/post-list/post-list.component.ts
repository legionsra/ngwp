import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { /* Observable, */ BehaviorSubject, Subscription, of } from 'rxjs';
// import { WordpressService } from '../../../services/wordpress.service';
import { IPostItem } from '../../../interfaces/ipost-item';
import { ConfigService } from '../../../services/config.service';
import { tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  @Input() columns = 6;
  @Input() nPosts = 40;
  @Input() gap = '16px';
  @Input() imgSizes = 'medium';
  private _posts = new BehaviorSubject<IPostItem[]>([]);
  private _isLoading = new BehaviorSubject<boolean>(true);

  private posts_susc: Subscription;
  
  isLoading = this._isLoading.asObservable();
  posts = this._posts.asObservable();
  // list = {
  //   columns: 4,
  //   gap: '16px'
  // };
  constructor(private _config: ConfigService) { }

  ngOnInit() {
    this.posts_susc = this._config.$confLoaded.pipe(
      tap(loaded => console.log(loaded)),
      switchMap(loaded => {
        console.log(loaded);
        
        return (loaded) ? this._config.getPostList(this.nPosts) : of([])}),
    ).subscribe((posts) => {
      // console.log(posts);
      this._isLoading.next(false);
      this._posts.next(posts);
      
    })
    // this._config.$confLoaded.subscribe(configReady => {
    //   if(configReady){
    //     // console.log('time to load posts')
    //     this.posts_susc = this._config.getPostList(this.nPosts).pipe(tap(() => this._isLoading.next(true)))
    //     .subscribe((posts: IPostItem[]) => {
    //       console.log(posts);
          
    //       this._isLoading.next(false);
    //       this._posts.next(posts);
    //     })
    //   } else {
    //     console.log('config not ready, waiting to load posts')
    //   }
    // })
    // this.posts = this.wp.getPosts();
  }
  ngOnDestroy() {
    this.posts_susc.unsubscribe();
  }

}
