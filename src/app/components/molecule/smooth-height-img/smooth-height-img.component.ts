import { Component, OnChanges, Input, ElementRef, HostBinding } from '@angular/core';
import { smoothHeight } from 'src/app/animations/minimize.animation';

@Component({
  selector: 'app-smooth-height-img',
  templateUrl: './smooth-height-img.component.html',
  styleUrls: ['./smooth-height-img.component.scss'],
  animations: [ smoothHeight ]
})
export class SmoothHeightImgComponent implements OnChanges {
  @Input() img;

  startHeight: number;

  constructor(private element: ElementRef) {}  
  
  @HostBinding('@grow') get grow() {
    return {value: this.img, params: {startHeight: this.startHeight}};
  }
  
  setStartHeight(){
    this.startHeight = this.element.nativeElement.clientHeight;
    console.log(this.startHeight);
    
  }
  
  ngOnChanges(){
    this.setStartHeight();
  }

}
