import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmoothHeightImgComponent } from './smooth-height-img.component';

describe('SmoothHeightImgComponent', () => {
  let component: SmoothHeightImgComponent;
  let fixture: ComponentFixture<SmoothHeightImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmoothHeightImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmoothHeightImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
