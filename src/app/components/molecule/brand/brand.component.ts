import { Component, OnInit, OnDestroy, Input } from '@angular/core';
// import { ConfigService } from '../../../services/config.service';
// import { BehaviorSubject, Subscription } from 'rxjs';
// import { switchMap } from 'rxjs/operators';
// import { IConfig } from '../../../interfaces/iconfig';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit, OnDestroy {
  // private _brand = new BehaviorSubject<any>({});
  // private _brand_susc: Subscription;
  // brand = this._brand.asObservable();
  @Input() brand;
  constructor(
    // private _config: ConfigService,
    private _h: HelperService
    ) { }

  ngOnInit() {
  
    
    
    // this._config.getConf().subscribe(({options: {ngwp_blogname: title}}) => this._brand.next({title}))
  }
  ngOnDestroy(): void {
      // this._brand_susc.unsubscribe();
  }
  onClickBrand(url: string, event: Event) {
    this._h.g.fluxNavigate(url,event);
  }
}
