import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetVertNocardListComponent } from './widget-vert-nocard-list.component';

describe('WidgetVertNocardListComponent', () => {
  let component: WidgetVertNocardListComponent;
  let fixture: ComponentFixture<WidgetVertNocardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetVertNocardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetVertNocardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
