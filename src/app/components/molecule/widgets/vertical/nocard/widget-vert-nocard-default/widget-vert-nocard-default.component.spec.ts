import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetVertNocardDefaultComponent } from './widget-vert-nocard-default.component';

describe('WidgetVertNocardDefaultComponent', () => {
  let component: WidgetVertNocardDefaultComponent;
  let fixture: ComponentFixture<WidgetVertNocardDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetVertNocardDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetVertNocardDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
