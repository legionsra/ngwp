import { Component, OnInit, Input } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-widget-vert-nocard-default',
  templateUrl: './widget-vert-nocard-default.component.html',
  styleUrls: ['./widget-vert-nocard-default.component.scss']
})
export class WidgetVertNocardDefaultComponent implements OnInit {
  @Input() html: string;
  constructor(
    public _h:HelperService
  ) { }

  ngOnInit() {
  }
  onClickAnchor( path : string, event : Event) {
    this._h.g.fluxNavigate(path,event);
  }
}
