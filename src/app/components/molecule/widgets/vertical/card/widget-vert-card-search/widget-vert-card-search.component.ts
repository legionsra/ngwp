import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-widget-vert-card-search',
  templateUrl: './widget-vert-card-search.component.html',
  styleUrls: ['./widget-vert-card-search.component.scss']
})
export class WidgetVertCardSearchComponent implements OnInit {
  @Input() html: string
  title = null;
  placeholder = 'Search';
  constructor() { }

  ngOnInit() {
    // screen-reader-text
    // console.log(this.html);
    const span = document.createElement('span');
    // const thisDomain = this.window.location.hostname;
    span.innerHTML = this.html;
    const localTitle = span.querySelector(".flux_header")
    this.title = localTitle !== null ? localTitle.innerHTML: this.title;
    // console.log(this.title);
    // console.log(this.html)
    const localPH = span.querySelector("label.screen-reader-text");
    this.placeholder = localPH !== null ? localPH.innerHTML : this.placeholder;
    // console.log(list);
    
    // const anchorList = Array.from(list.querySelectorAll("a"));
    
  }

}
