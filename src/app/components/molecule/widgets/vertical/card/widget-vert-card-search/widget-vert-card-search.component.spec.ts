import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetVertCardSearchComponent } from './widget-vert-card-search.component';

describe('WidgetVertCardSearchComponent', () => {
  let component: WidgetVertCardSearchComponent;
  let fixture: ComponentFixture<WidgetVertCardSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetVertCardSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetVertCardSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
