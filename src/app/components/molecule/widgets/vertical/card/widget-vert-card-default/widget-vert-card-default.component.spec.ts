import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetVertCardDefaultComponent } from './widget-vert-card-default.component';

describe('WidgetVertCardDefaultComponent', () => {
  let component: WidgetVertCardDefaultComponent;
  let fixture: ComponentFixture<WidgetVertCardDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetVertCardDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetVertCardDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
