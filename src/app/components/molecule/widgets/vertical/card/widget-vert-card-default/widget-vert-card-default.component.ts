import { Component, OnInit, Input } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-widget-vert-card-default',
  templateUrl: './widget-vert-card-default.component.html',
  styleUrls: ['./widget-vert-card-default.component.scss']
})
export class WidgetVertCardDefaultComponent implements OnInit {
  @Input() html: string;
  constructor(
    public _h: HelperService
  ) { }

  ngOnInit() {
  }
  onClickAnchor( path : string, event : Event) {
    this._h.g.fluxNavigate(path,event);
  }

}
