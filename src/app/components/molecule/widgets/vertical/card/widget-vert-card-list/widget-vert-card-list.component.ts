import { Component, OnInit, Input, Inject } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';
import { ConfigService } from 'src/app/services/config.service';
import { WINDOW } from 'src/app/providers/window.provider';

@Component({
  selector: 'app-widget-vert-card-list',
  templateUrl: './widget-vert-card-list.component.html',
  styleUrls: ['./widget-vert-card-list.component.scss']
})
export class WidgetVertCardListComponent implements OnInit {
  @Input() html: string
  list = []
  title: string;
  constructor(
    private _h: HelperService,
    private _config: ConfigService,
    @Inject(WINDOW) private window: Window
    ) { }

  ngOnInit() {

    const span = document.createElement('span');
    const thisDomain = this.window.location.hostname;
    span.innerHTML = this.html;
    this.title = span.querySelector(".flux_header").innerHTML;
    // console.log(this.title);
    // console.log(this.html)
    const list = span.querySelector("ul");
    const anchorList = Array.from(list.querySelectorAll("a"));
    
    anchorList.forEach(anchor => {
      // console.log(anchor);
      
      const urlStruc = this._h.g.getUrlParts(anchor.href)
      
      if(urlStruc.domain == thisDomain) {
        
        if(this._config.addURLToQueue(urlStruc.path)) {
          this.list.push({
            href: urlStruc.path,
            content: anchor.innerText,
            reload: false
          });
        } else {
          this.list.push({
            href: urlStruc.path,
            content: anchor.innerText,
            reload: true
          });
        }
      }

    })
    
  }
  onClickAnchor( path : string, event : Event, reload = false) {
    if(!reload) this._h.g.fluxNavigate(path,event);
  }

}
