import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetVertCardListComponent } from './widget-vert-card-list.component';

describe('WidgetVertCardListComponent', () => {
  let component: WidgetVertCardListComponent;
  let fixture: ComponentFixture<WidgetVertCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetVertCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetVertCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
