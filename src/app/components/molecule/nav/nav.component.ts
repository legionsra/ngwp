import { Component, OnInit, Input } from '@angular/core';

import { IMenu } from 'src/app/interfaces/imenu';
// import { ConfigService } from 'src/app/services/config.service';
// import { switchMap } from 'rxjs/operators';
import { HelperService } from 'src/app/services/helper.service';
// import { BehaviorSubject } from 'rxjs';



@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  // _menus = new BehaviorSubject<IMenu[]>([]);
  // elems = this._menus.asObservable();
  @Input() menus: IMenu[];
  // menu_susc : Subscription;
  constructor(
    // private _config:ConfigService,
    private _h: HelperService
  ) { }

  ngOnInit() {
    // console.log(this.menus)
    // this._menus.next(this.menus);
    
  }
  onClickMenu(url: string, event: Event) {
    this._h.g.fluxNavigate(url,event);
  }

}
