import { Component, OnInit, Input } from '@angular/core';
import { IPostItem } from '../../../interfaces/ipost-item';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {
  @Input() elemData: IPostItem;
  @Input() imgSize = 'thumbnail';
  
  constructor() { 
  }
  
  
  ngOnInit() {

  }

}
