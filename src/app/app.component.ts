import { Component, OnInit, HostBinding, ViewChild, ElementRef, Injector } from '@angular/core';
import { ConfigService } from './services/config.service';
import { createCustomElement } from '@angular/elements'

import { BehaviorSubject } from 'rxjs';
import { fadeAnimation } from './animations/fade.animation';
import { routesAnimation } from './animations/routes.animation';
import { delay } from 'rxjs/operators';
import { LinkComponent } from './elements/link/link.component';
// import { IPage } from './interfaces/ipage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    fadeAnimation,
    routesAnimation
  ]
})
export class AppComponent implements OnInit{
  public isLoading = new BehaviorSubject<boolean>(true);
  $isLoading = this.isLoading.asObservable();
  @ViewChild('main') mainEl : ElementRef; 
  constructor(
    private config: ConfigService,
    private injector: Injector,
    // private _router: Router
    // private _router2: Router
  ) {}
  @HostBinding( 'class.page-transitioned' ) pageAnimationFinished: boolean = false;
  pageTransitionFinish() {
      this.pageAnimationFinished = true;
      this.updateHeight(300);
      // console.log('updating height')
  }
  pageTransitionStart() {
    this.pageAnimationFinished = false;
    // this.updateHeight();
  }
  
  updateHeight(delay = 0) {
    const el = this.mainEl.nativeElement;

    setTimeout(() => {
      // console.log(el);
      const prevHeight = el.style.height;
      // console.log(`prev Height: ${prevHeight}`);
      el.style.height = 'auto';
      const newHeight = el.scrollHeight + 'px';
      // console.log(`new Height: ${newHeight}`);
      el.style.height = prevHeight;

      setTimeout(() => {
        el.style.height = newHeight;
      }, 50);
    }, delay);
  }

  ngOnInit(): void {
    // console.log(`Route URL: ${this.route.snapshot.params['url']}`);
    // this.route.url.subscribe((x) => {console.log(x); });
    console.log('APP ONINIT');
    const elemsLink = createCustomElement(LinkComponent, {injector: this.injector})
    customElements.define('app-link', elemsLink);
    // this._router.data.subscribe((input: {data: IPage}) => {
    //   console.log(input);
    //   // this.page = input.data;
    // })
    /* this._router.events.subscribe(event => {
      if (event instanceof ChildActivationEnd) {
        console.log(event.snapshot.firstChild.data);
        
      }
    }) */
    // console.log(this._router.snapshot.url);
    // console.log(this._router2.url);
    // const initialURL = '/'+this._router.snapshot.url.join('/');
    // console.log(initialURL);
    
    // this.updateHeight();
    // this.config.runApp();
    this.config.$confLoaded.pipe(delay(1400)).subscribe(loaded => this.isLoading.next(!loaded))
    /* this.config.requestConf(initialURL).subscribe((conf: IConfig) => {
      if(!(Object.keys(conf).length === 0 && conf.constructor === Object)){
        // console.log('isLoading')
        console.log(conf);
        this.isLoading.next(false)
        // setTimeout(()=>this.isLoading.next(false), 10000);
        
        // this.updateHeight(500);
      }
      else {
        // console.log('loading...')
      }
    }); */

  }
  // ngAfterViewInit() {
  //   this._router.params.subscribe(x =>console.log(x));
  //   console.log(this._router2);
  // }
  // ngOnChanges() {
  //   console.log('changes');
  //   this.updateHeight();
  // }
  // getPageData(outlet){
  //   // console.log(outlet)

  //   return outlet.activatedRouteData['data'];
  // }
}
