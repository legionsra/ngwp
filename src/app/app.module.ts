import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './modules/material.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/page/home/home.component';
import { WordpressService } from './services/wordpress.service';
import { PostListComponent } from './components/organism/post-list/post-list.component';
import { SinglePostComponent } from './components/molecule/single-post/single-post.component';
import { ConfigService } from './services/config.service';
import { UrlGuard } from './guards/url-guard.service';
import { HeaderComponent } from './components/organism/header/header.component';
import { HelperService } from './services/helper.service';
import { PageComponent } from './components/page/page/page.component';
import { PostsComponent } from './components/page/posts/posts.component';
import { SanitizeHtmlDirective } from './directives/sanitize-html.directive';
import { FooterComponent } from './components/organism/footer/footer.component';
import { BrandComponent } from './components/molecule/brand/brand.component';
import { NotFound404Component } from './components/page/not-found404/not-found404.component';
import { SidebarVerticalComponent } from './components/organism/sidebar-vertical/sidebar-vertical.component';
import { SafePipe } from './pipes/safe.pipe';
import { WINDOW_PROVIDERS } from './providers/window.provider';
// import {  DynamicComponentModule } from 'ng-dynamic';
// import { CommonModule } from '@angular/common';
import { Forbidden403Component } from './components/page/forbidden403/forbidden403.component';
import { WidgetVertCardDefaultComponent } from './components/molecule/widgets/vertical/card/widget-vert-card-default/widget-vert-card-default.component';
import { WidgetVertNocardDefaultComponent } from './components/molecule/widgets/vertical/nocard/widget-vert-nocard-default/widget-vert-nocard-default.component';
import { DynamicModule } from 'ng-dynamic-component';

import { WidgetVertCardListComponent } from './components/molecule/widgets/vertical/card/widget-vert-card-list/widget-vert-card-list.component';
import { WidgetVertNocardListComponent } from './components/molecule/widgets/vertical/nocard/widget-vert-nocard-list/widget-vert-nocard-list.component';
import { WidgetVertCardSearchComponent } from './components/molecule/widgets/vertical/card/widget-vert-card-search/widget-vert-card-search.component';
import { CategoryComponent } from './components/page/category/category.component';
import { LinkComponent } from './elements/link/link.component';
import { NavComponent } from './components/molecule/nav/nav.component';
import { SmoothHeightImgComponent } from './components/molecule/smooth-height-img/smooth-height-img.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostListComponent,
    SinglePostComponent,
    HeaderComponent,
    PageComponent,
    PostsComponent,
    SanitizeHtmlDirective,
    FooterComponent,
    BrandComponent,
    NotFound404Component,
    SidebarVerticalComponent,
    SafePipe,
    Forbidden403Component,
    WidgetVertCardDefaultComponent,
    WidgetVertNocardDefaultComponent,
    // WidgetVertCardRecentEntriesComponent,
    // WidgetVertNocardRecentEntriesComponent,
    // WidgetVertCardRecentCommentsComponent,
    // WidgetVertNocardRecentCommentsComponent,
    WidgetVertCardListComponent,
    WidgetVertNocardListComponent,
    WidgetVertCardSearchComponent,
    CategoryComponent,
    LinkComponent,
    NavComponent,
    SmoothHeightImgComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    MaterialModule,
    /* DynamicComponentModule.forRoot({
      imports: [
        CommonModule,
        MaterialModule,
      ]
    }), */
    DynamicModule.withComponents([
      WidgetVertCardDefaultComponent, 
      WidgetVertCardListComponent,
      WidgetVertCardSearchComponent,
      // WidgetVertCardRecentCommentsComponent,
      // WidgetVertCardRecentEntriesComponent,
      WidgetVertNocardDefaultComponent,
      // WidgetVertNocardRecentCommentsComponent,
      // WidgetVertNocardRecentEntriesComponent,
    ])

  ],
  providers: [
    WordpressService,
    ConfigService,
    UrlGuard,
    HelperService,
    WINDOW_PROVIDERS],
  bootstrap: [AppComponent],
  entryComponents: [
    HomeComponent,
    PageComponent,
    PostsComponent,
    NotFound404Component,
    Forbidden403Component,
    CategoryComponent,
    LinkComponent,
  ]
})
export class AppModule { }
