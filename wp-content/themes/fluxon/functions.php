<?php
// use WP_REST_Server;
// use WP_REST_Request;
// use WP_REST_Response;
// use WP_Error;
require get_template_directory() . '/inc/admin.php';
function ngwp_enqueue_dependencies()
{
    // wp_enqueue_style( 'icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', array(), '1.0.0', 'all');
    // wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500', array(), '1.0.0', 'all');
    wp_enqueue_style( 'styles', get_template_directory_uri() . '/build/styles.css', array(), '1.0.0', 'all');
    
    wp_enqueue_script('runtime', get_template_directory_uri() . '/build/runtime.js', array(), '1.0.0', true);
    wp_enqueue_script('polyfills', get_template_directory_uri() . '/build/polyfills.js', array(), '1.0.0', true);
    // wp_enqueue_script('styles', get_template_directory_uri() . '/build/styles.js', array(), '1.0.0', false);
    // wp_enqueue_script('vendor', get_template_directory_uri() . '/build/vendor.js', array(), '1.0.0', true);
    wp_enqueue_script('main', get_template_directory_uri() . '/build/main.js', array(), '1.0.0', true);
    
}

// wp_enqueue_scripts('ngwp_enqueue_dependencies');
add_action('wp_enqueue_scripts', 'ngwp_enqueue_dependencies');

/**
 * Add support for Menus and add two initial locations
 *
 * @return void
 */
function ngwp_menu_init()
{
    add_theme_support('menus');
    register_nav_menu('ngwp_header', 'This is the header location');
    register_nav_menu('ngwp_footer', 'This is the footer location');
}
add_action('init', 'ngwp_menu_init');

function ngwp_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Vertical sidebar', 'ngwp' ),
        'id'            => 'vertical_sidebar',
        'description' => __( 'Vertical customizable sidebar.', 'ngwp' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="flux_header">',
		'after_title'   => '</div>',
	) );

}
add_action( 'widgets_init', 'ngwp_widgets_init' );

/*
    * Enable support for Post Formats.
    *
    * See: https://codex.wordpress.org/Post_Formats
    */
// add_theme_support(
//     'post-formats', array(
//         'aside',
//         'image',
//         'video',
//         'quote',
//         'link',
//         'gallery',
//         'audio',
//     )
// );
    
// function ngwp_get_menu()
// {
//     # Change 'menu' to your own navigation slug.
//     // echo wp_get_nav_menu_items('header');
//     return wp_get_nav_menu_items('header3');
//     // return get_registered_nav_menus(  );
// }

/**
 * Get Menu By Location
 * @param   string    $theme_location    Theme location
 * @return  mixed                        Menu Object or false if not found
 */
function ngwp_get_menu_by_location($data)
{
    // return 'something';
    $theme_location = 'ngwp_' . $data['location'];
    $theme_locations = get_nav_menu_locations();
    // $theme_locations = get_registered_nav_menus();
    // return array_key_exists($theme_location, $theme_locations);
    // return $theme_locations;
    // return is_array($theme_locations);
    if( !array_key_exists($theme_location, $theme_locations)) return false;
    $menu_obj = get_term($theme_locations[$theme_location], 'nav_menu');
    // return $theme_location;
    // return $menu_obj;
    $menus = ($menu_obj && isset($menu_obj->name) /* && $menu_obj->data !== null */ ) ? wp_get_nav_menu_items($menu_obj->name): false;
    // $output = array();
    if($menus) {
        foreach ( $menus as $value ) {
            // echo $value->ID;
            // $array[] = $value->object ;
            // $value->data = ngwp_intREST_get_content( $value->object_id, $value->object );
            if(!($value->object == 'custom')){
                $value->data = ngwp_intREST_get_content( $value->object_id, $value->object );
                // $output[] = ngwp_intREST_get_content( $value->object_id, $value->object );  
            } 
            // else {
            //     $output[] = $value;
            // }
        }
        // $menus = $output;
    }
    return $menus;
    // if ($menu_obj && $menu_obj->data !== null ) {
    //     // return $menu_obj;
    //     return wp_get_nav_menu_items($menu_obj->name);
    // } else {
    //     // return array();
    //     return false
    //     return $menu_obj;
    // }
}


add_action('rest_api_init', function () {
    // echo wp_get_nav_menu_items('header_menu');
    register_rest_route('ngwp/v1', '/menu/(?P<location>[a-zA-Z0-9-]+)', array(
        'methods' => 'GET',
        'callback' => 'ngwp_get_menu_by_location',
        // 'args' => array()
    ));
});
add_action('rest_api_init', function () {
    register_rest_route('ngwp/v1', '/config', array(
        'methods' => 'GET',
        'callback' => 'ngwp_get_config',
        // 'args' => array()
    ));
});
// add_action('rest_api_init', function () {
//     register_rest_route('ngwp/v1', '/config/(?P<url>[a-zA-Z0-9-]+)', array(
//         'methods' => 'GET',
//         'callback' => 'ngwp_get_config',
//         // 'args' => array()
//     ));
// });
add_action('rest_api_init', function () {
    require_once('api/sidebar2.php');
      $class = new Sidebars_Controller;
      $class->register_routes();
});
add_action('rest_api_init', function () {
    register_rest_route('ngwp/v1', '/url', array(
        'methods' => 'POST',
        'callback' => 'ngwp_get_page_from_urls',
        // 'args' => array()
    ));
    register_rest_route('ngwp/v1', '/url', array(
        'methods' => 'GET',
        'callback' => 'ngwp_retrieve_content_URL',
        // 'args' => array()
    ));
});
// add_action('rest_api_init', function () {
    
// });
// add_action('rest_api_init', function () {
//     require_once('api/widget.php');
//       $class = new Widget();
//       $class->register_routes();
// });
function ngwp_get_page_from_urls($data) {
    // return $data->get_params();
    // return $data->get_params();
    // $url = ($data['url'] === null || $data['url'] == '/') ? false 
    //     : (gettype($data) === string) ? $data : $data['url'];
    $url = ($data['url'] == null || $data['url'] == '/') ? false : $data['url'];
    // return new WP_Error( 404 );
    // return $url;
    if($url) {
        if(is_array($url)){
            // return 'hello';
            // Case external API
            $response = array();
            foreach ( $url as $value ) {
                $object = new stdClass();
                $object->url = $value;
                // $object->content = ngwp_retrieve_content_URL($value);
                $object->content = ngwp_intREST_get_URL($value);
                $response[] = $object;
                // $response[] = $value;
                // $response[] = (object) array($value => ngwp_retrieve_content_URL($value));
            }
            $response = new WP_REST_Response($response, 200);
        }else {
            // Case internal API
            // $response = ngwp_retrieve_content_URL($url);
            $response = ngwp_intREST_get_URL($url);
        }
        return $response;
        
    }
    return new WP_Error( 400 );
    
}
function ngwp_retrieve_content_URL($data) {
    $url = ($data['url'] == null || $data['url'] == '/') ? false : $data['url'];

    // Case Posts
    $postID = url_to_postid($url);
    // return $postID;
    // Case Pages
    $pageID = get_page_by_path($url);
    // return $pageID;
    // Case Categories
    $categoryData = get_category_by_path($url,false);
        // return $categoryData;
    // return get_category($categoryData->cat_ID);
    $pageID = ($postID == 0 && $pageID !== null) ? $pageID->ID : null;
    // TODO: Create responses for archive urls
    $urlID = ($pageID !== null) ? $pageID 
        : ($postID !== 0) ? $postID
        : 0; 
    // $initialPage = $urlID;
    if($urlID !== 0) {

        $urlType = get_post_type($urlID);
        return ngwp_intREST_get_content( $urlID, $urlType );
    } else if($categoryData !== null) {
        // $categoryData->type = 'category';
        return $categoryData;
    } else {
        return new WP_Error( 'no_url', __( 'No path was found with the given URL', 'ngwp' ), array( 'status' => 404 ) );
    }
    return 'errorweird';
}
function ngwp_get_config($data) {
    $url = ($data['url'] == null || $data['url'] == '/') ? false : $data['url'];

    // $url = ($data['url'] == null) ? false 
    //     : (url_to_postid($data['url']) == 0) ? false
    //     : ngwp_internalREST_get_page( url_to_postid($data['url']) ) ;
    
    $homePageID = (int) get_option('page_on_front');
    $postsPageID = (int) get_option('page_for_posts');
    $config = array();
    $config['home_page'] = ( $homePageID > 0 ) ? ngwp_intREST_get_content( $homePageID, 'page' ) : false;
    $config['posts_page'] = ( $postsPageID > 0 ) ? ngwp_intREST_get_content( $postsPageID, 'page' ) : false;
    // $zq = get_page_by_path($url)->ID;
    // $config['urlnew'] = $zq;
    // $url = url_to_postid($url);
    // $initialPage = false;
    $initialPage = ($url) ? ngwp_intREST_get_URL($url) : false;
    /* if($url) {
        $postID = url_to_postid($url);
        $pageID = get_page_by_path($url)->ID;
        $urlID = ($pageID !== null) ? $pageID : $postID;
        $initialPage = $urlID;
        if($urlID !== 0) {
            if($urlID === $homePageID) {
                $initialPage = $config['home_page'];
            } else if($urlID === $postsPageID) {
                $initialPage = $config['posts_page'];
            } else {
                $urlType = get_post_type($urlID);
                $initialPage = ngwp_intREST_get_content( $urlID, $urlType );
            }
        } else {
            $initialPage = new WP_Error( 404 );
        }
    } */
    
    $config['menus']['header'] = ngwp_intREST_get_menu('header');
    $config['menus']['footer'] = ngwp_intREST_get_menu('footer');
    // $config['home_page'] = $homePageData;
    // if( is_a( $homePageData, '\WP_Post' ) ) {
    // // if( is_a( $homePageData, '\WP_Post' ) ) {
    //     $homePageData->post_content = apply_filters('the_content', $homePageData->post_content);
    //     $config['home_page'] = $homePageData;
    // } else $config['home_page'] = false;

    // if( is_a( $postsPageData, '\WP_Post' ) ) {
    //     // $postsPageData->post_content = apply_filters('the_content', $postsPageData->post_content);
    //     $config['posts_page'] = $postsPageData;
    // } else $config['posts_page'] = false;
 
    /* if($homePageID !== 0) {
        $homePageData = get_post($homePageID);
        // array_push($config, array('homepage' => $homePageData));
    } else {
        $config['homepage'] = false;
        // $homePageData = get_post($homePageID);
    } */
    // $config['posts'] = get_posts();
    $options = wp_load_alloptions();
    $config['options']['ngwp_site_url'] = $options['siteurl'];
    $config['options']['ngwp_site_home'] = $options['home'];
    $config['options']['ngwp_blogname'] = $options['blogname'];
    $config['options']['ngwp_blogdesc'] = $options['blogdescription'];
    $config['options']['ngwp_posts'] = $options['posts_per_page'];

    $config['options']['ngwp_cache'] = get_option('fluxon_cache', '');
    $config['options']['ngwp_async'] = get_option('fluxon_async', '');
    $config['options']['ngwp_delay_async'] = get_option('fluxon_async_debounce_countdown', '3000');
    $config['options2'] = $options;
    // $config['url'] =  url_to_postid('/pagina-ejemplo');
    // $config['url3'] =  url_to_postid('/2018/08/08/blsablafsa');
    // $config['url2'] =  url_to_postid('2018/07/25/hola-mundo');
    $config['initial_page'] = $initialPage;
    $config['sidebars']['vertical'] = ngwp_intREST_get_sidebar('vertical_sidebar');
    
    //  $config['homePageID'] = get_post(intval(get_option('page_on_front')));
    //  $config['postsPageID'] = get_post(intval(get_option('page_for_posts')));


    // return new \WP_REST_Response( $data, 200 ); 
    // return $data;
    return $config;
    // return get_option('page_for_posts');
}

function ngwp_intREST_get_content($id, $type) {
    $request = new WP_REST_Request( 'GET', '/wp/v2/'. $type .'s/' . $id );
    // $request->set_query_params( [ 'per_page' => 12 ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, true );
    // $json = wp_json_encode( $data );
    return $data;
}
function ngwp_intREST_get_URL($url) {
    
    $request = new WP_REST_Request( 'GET', '/ngwp/v1/url');
    $request->set_query_params( [ 'url' => $url ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );
    // $json = wp_json_encode( $data );
    return $data;
}
function ngwp_intREST_get_menu($location) {
    $request = new WP_REST_Request( 'GET', '/ngwp/v1/menu/' . $location );
    // $request->set_query_params( [ 'per_page' => 12 ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );
    // $json = wp_json_encode( $data );
    return $data;
}
function ngwp_intREST_get_sidebar($id) {
    $request = new WP_REST_Request( 'GET', '/ngwp/v1/sidebars/'. $id );
    // $request->set_query_params( [ 'per_page' => 12 ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, true );
    // $json = wp_json_encode( $data );
    return $data;
}
/**
 * Debug to the user console
 *
 * @param string $data
 * @return void
 */
function debug_to_console($data)
{
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function ngwp_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'ngwp_post_thumbnails' );






/**

 * Añadir soporte para las funcionalidades de Gutenberg

 */

function theme_slug_gutenberg_support() {



	// Theme support custom color palette

	add_theme_support( 'editor-color-palette', array(

		array(

			'name'  => esc_html__( 'Primario', 'theme-slug' ),

			'slug'  => 'primario',

			'color' => '#006699',

		),

		array(

			'name'  => esc_html__( 'Contraste', 'theme-slug' ),

			'slug'  => 'contraste',

			'color' => '#f4e08a',

		),

		array(

			'name'  => esc_html__( 'Gris Claro', 'theme-slug' ),

			'slug'  => 'gris-claro',

			'color' => '#e5e5e5',

		),

		array(

			'name'  => esc_html__( 'Gris Oscuro', 'theme-slug' ),

			'slug'  => 'gris-oscuro',

			'color' => '#5a5a5a',

		),

	) );



	// Desabilitar soporte para colores personalizados

	add_theme_support( 'disable-custom-colors' );

}
add_theme_support( 'editor-styles' );

add_action( 'after_setup_theme', 'theme_slug_gutenberg_support' );