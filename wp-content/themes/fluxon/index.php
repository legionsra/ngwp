<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>
    <?php bloginfo('name'); ?> |
    <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
  </title>
  <?php wp_head();?>

  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/build/styles.d76febf6214ac4049288.css"></head> -->

<body>
  <app-root>
    <?php
if ( have_posts() ) :
    
    /* Start the Loop */
    while ( have_posts() ) : the_post();
    
    /*
    * Include the Post-Format-specific template for the content.
    * If you want to override this in a child theme, then include a file
    * called content-___.php (where ___ is the Post Format name) and that will be used instead.
    */
    // get_template_part( 'template-parts/post/content', get_post_format() );
    
endwhile;

// the_posts_pagination( array(
    // 	'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
    // 	'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
    // 	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
    // ) );
    
    else :
        
        // get_template_part( 'template-parts/post/content', 'none' );
        ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <header class="entry-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <?php ?>
        <!-- twentyseventeen_edit_link( get_the_ID() );  -->
        <?php ?>
      </header><!-- .entry-header -->
      <div class="entry-content">
        <?php
        the_content();
        
        wp_link_pages( array(
            'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
            'after'  => '</div>',
            ) );
            ?>
      </div><!-- .entry-content -->
    </article><!-- #post-## -->
    <?php
        endif;
        ?>
  </app-root>

  <?php wp_footer() ?>
</body>

</html>
