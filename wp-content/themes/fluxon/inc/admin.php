<?php

/**
 * @Package fluxontheme
 * 
 *  ======================
 *      ADMIN PAGE
 *  ======================
 *
 */

 function fluxon_add_admin_page() {
    // Generate Fluxon Admin Page
    add_menu_page('Fluxon Theme Options', 'Fluxon', 'manage_options', 'fluxon_theme', 'fluxon_theme_create_page', 'dashicons-admin-customizer', 3);
    // Generate Fluxon Validation Admin Page
    add_submenu_page('fluxon_theme', 'Fluxon Validation', 'Validation', 'manage_options', 'fluxon_theme', 'fluxon_theme_create_page');
	// Generate Fluxon Settings Admin Page
    add_submenu_page('fluxon_theme', 'Fluxon Settings', 'Settings', 'manage_options', 'fluxon_theme_settings', 'fluxon_theme_settings_page');

    //Activate custom settings
	add_action( 'admin_init', 'fluxon_custom_settings' );
 }
 add_action('admin_menu', 'fluxon_add_admin_page');

 function fluxon_theme_create_page() {
	 echo 'Validation Page';
}

function fluxon_theme_settings_page() {


	require_once( get_template_directory() . '/inc/templates/admin-general.php' );
 }


 function fluxon_custom_settings() {
	register_setting( 'fluxon-settings-group', 'fluxon_cache' );
	register_setting( 'fluxon-settings-group', 'fluxon_async');
	register_setting( 'fluxon-settings-group', 'fluxon_async_debounce_countdown' );
	register_setting( 'fluxon-settings-style-group', 'fluxon_widget_card' );

	
	add_settings_section( 'fluxon-general-options', __('Flux general settings', '__fluxon__'), 'fluxon_general_section', 'fluxon_theme_settings');
	add_settings_section( 'fluxon-general-style-options', __('Flux style settings', '__fluxon__'), 'fluxon_general_style_section', 'fluxon_theme_settings');
	
	add_settings_field( 'general-cache', __('Enable Flux cache', '__fluxon__'), 'fluxon_general_cache', 'fluxon_theme_settings', 'fluxon-general-options');
	add_settings_field( 'general-async', __('Enable Flux Navigation', '__fluxon__'), 'fluxon_general_async', 'fluxon_theme_settings', 'fluxon-general-options');
	add_settings_field( 'general-async-debounce', __('Flux Navigation debounce', '__fluxon__'), 'fluxon_general_async_debounce', 'fluxon_theme_settings', 'fluxon-general-options');
	add_settings_field( 'general-widget-card', __('Widget style card', '__fluxon__'), 'fluxon_general_style_widget_card', 'fluxon_theme_settings', 'fluxon-general-style-options');
	// add_settings_field( 'general-facebook', 'Facebook handler', 'fluxon_general_facebook', 'fluxon_theme_settings', 'fluxon-general-options');
	// add_settings_field( 'general-gplus', 'Google+ handler', 'fluxon_general_gplus', 'fluxon_theme_settings', 'fluxon-general-options');
}

function fluxon_general_section() {
    echo __('Features to enhance user experience through the manipulation of the browser behaviour.', '__fluxon__');
}
function fluxon_general_style_section() {
    echo __('Features to changes the style of the website.', '__fluxon__');
}

function fluxon_general_cache() {
	$cache = esc_attr( get_option( 'fluxon_cache' ) );
	echo '<input name="fluxon_cache" type="checkbox" value="1" '
	. checked( 1, $cache, false )
	.'  /><p class="description">'
		. __(
<<<MARKER
Enables the visitor's browser to store the 
entire content of the pages requested, this 
improves the user experience even after 
they return to the website. This will also 
reduce radically the number of request to 
the server.
MARKER
		,'__fluxon__') . '</p>';
}
function fluxon_general_async() {
	$async = esc_attr( get_option( 'fluxon_async' ) );

	echo '<input name="fluxon_async" type="checkbox" value="1"'
	. checked( 1, $async, false )
	.'  /><p class="description">'
	. __(
<<<MARKER
Enables the browser to pre-fetch 
asynchronously any route available 
in the actual page independently 
of the visitor's behaviour. 
All possibles routes are fetched in
one single call.
MARKER
	,'__fluxon__') . '</p>';
}
function fluxon_general_async_debounce() {
    $debounceAsync = esc_attr( get_option( 'fluxon_async_debounce_countdown' ) );
	echo '<input type="number" name="fluxon_async_debounce_countdown" value="'.$debounceAsync.'" placeholder="3000" /><p class="description">'
	. __(
<<<MARKER
Miliseconds to wait after new page is loaded 
to request asynchronously any available route. 
Default is 3000ms.
MARKER
	,'__fluxon__') . '</p>';
}
function luxon_general_style_widget_card() {
    $widgetCard = esc_attr( get_option( 'fluxon_widget_card' ) );
	echo '<input name="fluxon_widget_card" type="checkbox" value="1"'
	. checked( 1, $async, false )
	.'  /><p class="description">'
	. __(
<<<MARKER
Activate visualization of material cards on widgets of the sidebar.
MARKER
	,'__fluxon__') . '</p>';
}