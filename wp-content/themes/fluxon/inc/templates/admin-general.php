<h1><?php echo __('Fluxon Settings', '__fluxon__'); ?></h1>

<?php settings_errors(); ?>
<form method="post" action="options.php">
	<?php settings_fields( 'fluxon-settings-group' ); ?>
	<?php do_settings_sections( 'fluxon_theme_settings' ); ?>
	<?php submit_button(); ?>
</form>